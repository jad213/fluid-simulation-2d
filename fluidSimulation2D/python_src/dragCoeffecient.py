import fluidSimulation2D as fs
from matplotlib import pyplot as plt
import numpy as np

f = fs.fluid2D()
b = fs.boundary2D()
sp = fs.sim2D()

A = 2e-3 # per unit length

centre = np.array([5e-3, 5e-3])

f.readFromFile('txt/f.txt')
b.readFromFile('txt/b.txt')
sp.readFromFile('simulation_parameters.txt')

offsets = [np.array([o, 1.1e-3]) for o in np.arange(1.1e-3, 4e-3, 0.1e-3)]

drag_coeffs = [abs(f.SFME(centre - offset, centre + offset)[0] / (0.5 * f.density * sp.initialV[0]**2 * A)) for offset in offsets]

plt.plot(offsets, drag_coeffs)
plt.xlabel('box width / 2')
plt.ylabel('Drag coeffecient')
plt.title('Drag coeffecient as the control volume is varied in size')
plt.show()