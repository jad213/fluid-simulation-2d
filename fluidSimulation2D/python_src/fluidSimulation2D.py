# file for the python classes
# these classes are used for running the simulation, visulising the results, and post processing

import numpy as np
from matplotlib import pyplot as plt
import subprocess

class boundary2D:
    def __init__(self):
        self.points = np.zeros([10, 2])

    def readFromFile(self, filename):
        file = open(filename, 'r')
        lines = file.readlines()
        string = "".join(lines).replace("\n", " ")
        self.points = np.fromstring(string, sep=" ").reshape([len(lines), 2])

    def writeToFile(self, filename):
        file = open(filename, 'w')
        stringOut = ''
        for i in range(np.shape(self.points)[0]):
            stringOut += str(self.points[i][0]) + ' ' + str(self.points[i][1]) + '\n'

        file.write(stringOut)
        file.close()

    def plotBound(self, show=False):
        plt.plot(self.points[:, 0], self.points[:, 1], color='black')

        if show:
            plt.show()

class fluid2D:
    def __init__(self):
        self.xPoints = 10
        self.yPoints = 10
        self.deltaX = 1.0
        self.deltaY = 1.0
        self.density = 1000
        self.viscosity = 0
        self.p = np.zeros([10, 10])
        self.v = np.zeros([10, 10, 2])

    def readFromFile(self, filename):
        file = open(filename, 'r')
        lines = file.readlines()
        self.xPoints = int(lines[0])
        self.yPoints = int(lines[1])
        self.deltaX = float(lines[2])
        self.deltaY = float(lines[3])
        self.density = float(lines[4])
        self.viscosity = float(lines[5])

        pString = lines[6:6 + self.yPoints]
        pString = ''.join(pString)
        self.p = np.fromstring(pString, sep=' ').reshape(self.yPoints, self.xPoints).T

        vString = lines[6 + self.yPoints:6 + 2 * self.yPoints]
        vString = ''.join(vString)
        vString = vString.replace('[', '')
        vString = vString.replace(']', '')
        vString = vString.replace(',', ' ')

        self.v = np.transpose(np.fromstring(vString, sep=' ').reshape(self.yPoints, self.xPoints, 2), (1, 0, 2))

    def xDerivative(self, field):
        ''' returns the derivative of the field wrt x '''
        # midpoint method
        derivative = (np.roll(field, -1, 0) - np.roll(field, 1, 0)) * (0.5 / self.deltaX)
        # correct edge cases
        derivative[ 0, :] = derivative[ 1, :]
        derivative[-1, :] = derivative[-2, :]

        return derivative

    def yDerivative(self, field):
        ''' returns the derivative of the field wrt y '''
        # midpoint method
        derivative = (np.roll(field, -1, 1) - np.roll(field, 1, 1)) * (0.5 / self.deltaY)
        # correct edge cases
        derivative[:,  0] = derivative[:,  1]
        derivative[:, -1] = derivative[:, -2]

        return derivative

    def xSecondDerivative(self, field):
        ''' returns the second derivative of the field wrt x '''
        # central difference method
        d2Fdx2 = (np.roll(field, -1, 0) - field * 2 + np.roll(field, 1, 0)) * self.deltaX**-2
        # correct edge cases
        d2Fdx2[ 0, :] = d2Fdx2[ 1, :]
        d2Fdx2[-1, :] = d2Fdx2[-2, :]

        return d2Fdx2

    def ySecondDerivative(self, field):
        ''' returns the second derivative of the field wrt y '''
        # central difference method
        d2Fdy2 = (np.roll(field, -1, 1) - field * 2 + np.roll(field, 1, 1)) * self.deltaY**-2
        # correct edge cases
        d2Fdy2[:,  0] = d2Fdy2[:,  1]
        d2Fdy2[:, -1] = d2Fdy2[:, -2]

        return d2Fdy2

    def gradP(self):
        ''' Caculates grad(pressure). Returns: x-component, y-component '''
        gradp = np.zeros([self.xPoints, self.yPoints, 2])
        gradp[:, :, 0] = self.xDerivative(self.p)
        gradp[:, :, 1] = self.yDerivative(self.p)
        return gradp

    def div(self, vector):
        return self.xDerivative(vector[:, :, 0]) + self.yDerivative(vector[:, :, 1])

    def divV(self):
        ''' returns divergance(pressure) '''
        return self.div(self.v)

    def laplacianV(self):
        ''' returns the laplacian of the velocity field as: x-component, y-component '''
        return self.xSecondDerivative(self.v) + self.ySecondDerivative(self.v)

    def laplacianP(self):
        ''' returns the laplacian of the pressure '''
        return self.xSecondDerivative(self.p) + self.ySecondDerivative(self.p)

    def vDotGradV(self):
        ''' returns: x-component, y-component of v dot grad(v) '''
        vDotGradV = np.zeros([self.xPoints, self.yPoints, 2])
        # x component
        vDotGradV[:, :, 0] = self.v[:, :, 0] * self.xDerivative(self.v[:, :, 0]) + self.v[:, :, 1] * self.yDerivative(self.v[:, :, 0])
        # y component
        vDotGradV[:, :, 1] = self.v[:, :, 0] * self.xDerivative(self.v[:, :, 1]) + self.v[:, :, 1] * self.yDerivative(self.v[:, :, 1])

        return vDotGradV

    def curl(self, vector):
        return self.xDerivative(vector[:, :, 1]) - self.yDerivative(vector[:, :, 0])

    def velError(self):
        velMag = np.sqrt(self.v[:, :, 0]**2 + self.v[:, :, 1]**2)
        avV = np.mean(velMag)
        return self.divV() * np.sqrt(self.deltaX**2 + self.deltaY**2) / avV

    def plotProfileAbs(self, field, axis=0, title = '', show=False):
            if axis == 0:
                x = np.arange(0, self.xPoints) * self.deltaX
                plt.plot(x, field[:, int(0.5 * self.yPoints)])
                plt.xlabel('X-axis')
                plt.ylabel('Field-axis')
            else:
                x = np.arange(0, self.xPoints) * self.deltaY
                plt.plot(x, field[int(0.5 * self.yPoints), :])
                plt.xlabel('Y-axis')
                plt.ylabel('Field-axis')
                
            plt.title(title)

            if show:
                plt.show()
            
            return

    def plotProfileXY(self, vecField, axis=0, title = '', show=False):
        self.plotProfileAbs(vecField[:, :, 0], axis, title)
        self.plotProfileAbs(vecField[:, :, 1], axis, title, show)
        return

    def plotContour(self, field, contourLines=10, title='', show=False, filled=False):
        A = np.arange(0, self.xPoints) * self.deltaX
        B = np.arange(0, self.yPoints) * self.deltaY

        AA, BB = np.meshgrid(A, B)

        plt.xlabel('x-axis')
        plt.ylabel('y-axis')
        plt.title(title)

        if filled:
            plt.contourf(AA.T, BB.T, field, levels=contourLines)
        else:
            CS = plt.contour(AA.T, BB.T, field, levels=contourLines)
            plt.clabel(CS, inline=1)

        if show:
            plt.show()
        return

    def plotQuiver(self, xPoints, yPoints, show=False):
        xSlice = int(self.xPoints / xPoints)
        ySlice = int(self.yPoints / yPoints)

        
        A = (np.arange(0, self.xPoints) * self.deltaX)[::xSlice]
        B = (np.arange(0, self.yPoints) * self.deltaY)[::ySlice]

        AA, BB = np.meshgrid(A, B)

        plt.quiver(AA.T, BB.T, self.v[:, :, 0][::xSlice, ::ySlice], self.v[:, :, 1][::xSlice, ::ySlice])

        if show:
            plt.show()
        return

    def checkPositionInField(self, p):
        ''' Returns True if the (x, y) point is inside the field, False if not'''
        if p[0] <= 0 or p[0] >= (self.xPoints - 1) * self.deltaX:
            return False
        elif p[1] <= 0 or p[1] >= (self.yPoints - 1) * self.deltaY:
            return False
        else:
            return True

    def interpolateVelocity(self, p):
        ''' linearly interpolates the velocity for a position between grid points.
        returns: vX, vY '''

        try:
            # find normalised positions
            pNorm = np.zeros(2)
            pNorm[0] = p[0] / self.deltaX
            pNorm[1] = p[1] / self.deltaY
            # find index of the vX and vY values
            vXi = int(np.floor(pNorm[0]))
            vYi = int(np.floor(pNorm[1]))

            xWeight = pNorm[0] - vXi
            yWeight = pNorm[1] - vYi

            vAv1 = (1 - xWeight) * self.v[vXi, vYi    ] + xWeight * self.v[vXi + 1, vYi    ]
            vAv2 = (1 - xWeight) * self.v[vXi, vYi + 1] + xWeight * self.v[vXi + 1, vYi + 1]
            vAv3 = (1 - yWeight) * vAv1 + yWeight * vAv2
        
        except:
            print('Attempted imterpolation of field outside of field. Position: ', p[0], p[1])
            vAv3 = np.zeros(2)

        return vAv3

    def streamline(self, startPos, step, maxSteps, minVelocity):
        ''' returns the points of a velocity streamline that starts at startPosition '''

        streamLinePoints = np.zeros([maxSteps + 1, 2])

        # set start position
        streamLinePoints[0] = startPos

        it = 0
        while it < maxSteps:
            # get current position
            p = streamLinePoints[it]

            # check current position is in the field
            if self.checkPositionInField(p):
                # find the field velocity at the current position and then normalise it if its > minVelocity
                v = self.interpolateVelocity(p)
                vMag = np.sqrt(v[0] ** 2 + v[1] ** 2)

                # if the velocity is lager than minVelcoity, normalise it w.r.t coordinate system, otherwise make it zero
                # multiplying by deltaX just makes the streamline steps on the same order of magnitude as the grid, you could use deltaY here instead
                if vMag > minVelocity:
                    v *= (self.deltaX / vMag)
                else:
                    # when the velocity reaches zero, stop the streamline as it stagnates
                    return streamLinePoints[:it + 1]

                # set next point based on direction of velocity
                streamLinePoints[it + 1] = p + v * step

            else:
                # the streamline has left the grid, so end it
                return streamLinePoints[:it]
            
            # exit condition for while loop
            it += 1

        #rint('While loop finished in streamline')

        if self.checkPositionInField(streamLinePoints[-1]):
            return streamLinePoints
        else:
            return streamLinePoints[:-1]

    def plotFlow(self, startX, fieldlines, step, maxStreamlineSteps, minVelocity, show=False, colour='blue'):
        '''plots the streamlines of the velocity field'''

        startPoints = np.zeros([fieldlines, 2])
        startPoints[:, 0] = np.ones(fieldlines) * startX
        startPoints[:, 1] = np.linspace(0, self.yPoints - 1, fieldlines + 2)[1:-1] * self.deltaY

        for i in range(fieldlines):
            streamLine = self.streamline(startPoints[i], step, maxStreamlineSteps, minVelocity)
            plt.plot(streamLine[:, 0], streamLine[:, 1], color=colour)

        if show:
            plt.show()
        return

    def plotGridPoints(self):
        A = np.arange(0, self.xPoints) * self.deltaX
        B = np.arange(0, self.yPoints) * self.deltaY

        AA, BB = np.meshgrid(A, B)

        for i in range(self.yPoints):
            plt.scatter(AA[i], BB[i])

    def plotContourSubplots(self, b, contourLines = 10, filled=False):
        ''' plots the 4 different contour plots '''
        # set up sub plot then plot the 4 contours
        fig = plt.figure()

        plt.subplot(2, 2, 1)
        self.plotContour(self.p, contourLines=contourLines, title='Pressure', filled=filled)
        self.plotFlow(0.001e-3, 20, 1, 10000, 0)
        b.plotBound()

        plt.subplot(2, 2, 2)
        self.plotContour(np.sqrt(self.v[:, :, 0] ** 2 + self.v[:, :, 1] ** 2), contourLines=contourLines, title='|Velocity|', filled=filled)
        self.plotQuiver(30, 30)
        b.plotBound()

        plt.subplot(2, 2, 3)
        self.plotContour(self.velError(), contourLines=contourLines, title='Velocity Error', filled=filled)
        b.plotBound()

        plt.subplot(2, 2, 4)
        #self.plotProfileAbs(self.velError(), axis=0)
        #self.plotProfileAbs(self.velError(), axis=1, title='Velocity Error at x = w / 2 & y = w / 2')
        #eta = self.viscosity * self.laplacianV() - self.density * self.vDotGradV()
        self.plotContour(self.curl(self.gradP()), contourLines=contourLines, title='curl(grad(p))', filled=filled)

        #b.plotBound()

        # adjust plot spacing
        plt.subplots_adjust(hspace=0.4)

        # make subplot full screen
        mng = plt.get_current_fig_manager()
        mng.window.showMaximized()

        # show plot
        plt.show()
        return

    def plotProfileSubplots(self, axis=0):
        ''' plots the 4 different profile plots '''
        # set up sub plot then plot the 4 contours
        fig = plt.figure()
        plt.subplot(2, 2, 1)
        self.plotProfileAbs(self.p, axis=axis, title='Pressure')
        plt.subplot(2, 2, 2)
        self.plotProfileAbs(self.velError(), axis=axis, title='Velocity Error')
        plt.subplot(2, 2, 3)
        self.plotProfileXY(self.v, axis=axis, title='Velocity at y = width/2')
        plt.subplot(2, 2, 4)
        #self.plotProfileXY(self.v, axis=1 - axis, title='Velocity at x = width/2')
        eta = self.viscosity * self.laplacianV() - self.density * self.vDotGradV()
        self.plotProfileAbs(self.curl(self.gradP()), axis=axis, title='Curl(grad(p))')
        #self.plotProfileAbs(eta, axis=axis, title='eta')

        # adjust plot spacing
        plt.subplots_adjust(hspace=0.4)

        # make subplot full screen
        mng = plt.get_current_fig_manager()
        mng.window.showMaximized()

        # show plot
        plt.show()
        return

    def Re(self, v, d):
        return self.density * v * d / self.viscosity

    def SFME(self, pmin, pmax, splitForce = False):
        ''' Applies the steady flow momentum equation on a rectangular control volume
        with bottom left corner pmin and top right corner pmax '''
        ix0 = int(pmin[0] / self.deltaX)
        ix1 = int(pmax[0] / self.deltaX)
        iy0 = int(pmin[1] / self.deltaY)
        iy1 = int(pmax[1] / self.deltaY)

        # define variables for clarity
        vx = self.v[:, :, 0]
        vy = self.v[:, :, 1]
        rho = self.density

        # x direction
        Fx_xmin = np.sum(-rho * np.power(vx[ix0, iy0:iy1], 2) - self.p[ix0, iy0:iy1]) * self.deltaX
        Fx_xmax = np.sum( rho * np.power(vx[ix1, iy0:iy1], 2) + self.p[ix1, iy0:iy1]) * self.deltaX
        Fx_ymin = np.sum(-rho * vx[ix0:ix1, iy0] * vy[ix0:ix1, iy0]) * self.deltaY
        Fx_ymax = np.sum( rho * vx[ix0:ix1, iy1] * vy[ix0:ix1, iy1]) * self.deltaY
        Fx = Fx_xmin + Fx_xmax + Fx_ymin + Fx_ymax

        # y direction
        Fy_xmin = np.sum(-rho * vx[ix0, iy0:iy1] * vy[ix0, iy0:iy1]) * self.deltaX
        Fy_xmax = np.sum( rho * vx[ix1, iy0:iy1] * vy[ix1, iy0:iy1]) * self.deltaX
        Fy_ymin = np.sum(-rho * np.power(vy[ix0:ix1, iy0], 2) - self.p[ix0:ix1, iy0]) * self.deltaY
        Fy_ymax = np.sum( rho * np.power(vy[ix0:ix1, iy1], 2) + self.p[ix0:ix1, iy1]) * self.deltaY
        Fy = Fy_xmin + Fy_xmax + Fy_ymin + Fy_ymax

        # viscous forces
        Fv_xmin = -np.sum(vy[ix0 + 1, iy0:iy1] - vy[ix0, iy0:iy1]) * (self.viscosity * self.deltaY / self.deltaX)
        Fv_xmax =  np.sum(vy[ix1 + 1, iy0:iy1] - vy[ix1, iy0:iy1]) * (self.viscosity * self.deltaY / self.deltaX)
        Fvy = Fv_xmin + Fv_xmax

        Fv_ymin = -np.sum(vx[ix0:ix1, iy0 + 1] - vx[ix0:ix1, iy0]) * (self.viscosity * self.deltaX / self.deltaY)
        Fv_ymax =  np.sum(vx[ix0:ix1, iy1 + 1] - vx[ix0:ix1, iy1]) * (self.viscosity * self.deltaX / self.deltaY)
        Fvx = Fv_ymin + Fv_ymax

        if splitForce:
            return np.array([Fx, Fy]), np.array([Fvx, Fvy])
        else:
            return np.array([Fx + Fvx, Fy + Fvy])

class sim2D:
    def __init__(self, width=[10e-3, 10e-3], spacing=[1e-3, 1e-3], density=1000, viscosity=1e-5, deltaT=1e-5, maxAvError=1e-7, pseudoPressureIt=10, realPressureIt=100, smoothIt=1, maxIt=100, initialP=0, initialV=[1, 0], boundFilename='', fluidReadFilename='', fluidWriteFilename='', executableDir=''):
        self.xPoints = round(width[0] / spacing[0]) + 1
        self.yPoints = round(width[1] / spacing[1]) + 1
        self.deltaX = spacing[0]
        self.deltaY = spacing[1]
        self.density = density
        self.viscosity = viscosity
        self.deltaT = deltaT
        self.maxAvError = maxAvError
        self.pseudoPressureIt = pseudoPressureIt
        self.realPressureIt = realPressureIt
        self.smoothIt = smoothIt
        self.maxIt = maxIt
        self.initialP = initialP
        self.initialV = initialV
        self.boundFilename = boundFilename
        self.fluidReadFilename = fluidReadFilename
        self.fluidWriteFilename = fluidWriteFilename
        self.executableDir = executableDir

    def writeToFile(self, filename):
        file = open(filename, 'w')
        file.write(
            str(self.xPoints) + "\n" +
            str(self.yPoints) + "\n" +
            str(self.deltaX) + "\n" +
            str(self.deltaY) + "\n" +
            str(self.density) + "\n" +
            str(self.viscosity) + "\n" +
            str(self.deltaT) + "\n" +
            str(self.maxAvError) + "\n" +
            str(self.pseudoPressureIt) + "\n" +
            str(self.realPressureIt) + "\n" +
            str(self.smoothIt) + "\n" +
            str(self.maxIt) + "\n" +
            str(self.initialP) + "\n" +
            str(self.initialV[0]) + " " + str(self.initialV[1]) + "\n" +
            self.boundFilename + "\n" +
            self.fluidReadFilename + "\n" +
            self.fluidWriteFilename + "\n" +
            self.executableDir
        )
        file.close()

    def readFromFile(self, filename):
        file = open(filename, 'r')
        lines = file.readlines()
        self.xPoints = int(lines[0])
        self.yPoints = int(lines[1])
        self.deltaX = float(lines[2])
        self.deltaY = float(lines[3])
        self.density = float(lines[4])
        self.viscosity = float(lines[5])
        self.deltaT = float(lines[6])
        self.maxAvError = float(lines[7])
        self.pseudoPressureIt = int(lines[8])
        self.realPressureIt = int(lines[9])
        self.smoothIt = int(lines[10])
        self.maxIt = int(lines[11])
        self.initialP = float(lines[12])
        self.initialV = np.fromstring(lines[13], sep=" ")
        self.boundFilename = lines[14]
        self.fluidReadFilename = lines[15]
        self.fluidWriteFilename = lines[16]
        self.executableDir = lines[17]
        file.close()

    def runSim(self):
        self.writeToFile("simulation_parameters.txt")
        subprocess.call(self.executableDir + "/fluidSimulation2d.exe", shell=True)
        f = fluid2D()
        f.readFromFile(self.fluidWriteFilename)
        return f
