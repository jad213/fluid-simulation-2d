import fluidSimulation2D as fs
import numpy as np

# simulation parameters
width = [10e-3, 10e-3]
spacing = [0.05e-3, 0.05e-3]
density = 1000
viscosity = 10e-5
deltaT = 0.000001
errorTol = 1e-7
pseudoPressureIt = 10
realPressureIt = 1000
smoothIt = 1
maxIt = 2000
initialP = 0
initialV = [10, 0]
boundFilename = "txt/b.txt"
fluidReadFilename = ""   #'txt/f.txt'
fluidWriteFilename = "txt/f.txt"
executableDir = "C:/Users/james/projects/fluidSimulation2D/bin/Win32/Release/"

# polar function
a = 1e-3
def r(theta):
    return a * (1 + 0.2 * np.cos(8 * theta))

p = 0.5 * np.array(width)
deltaTheta = deltaTheta = 2 * spacing[0] / a

# create cylinder
x = np.array([p[0] + r(theta) * np.cos(theta) for theta in np.arange(0, 2 * np.pi, deltaTheta)])
y = np.array([p[1] + r(theta) * np.sin(theta) for theta in np.arange(0, 2 * np.pi, deltaTheta)])
b = fs.boundary2D()
b.points = np.zeros([np.shape(x)[0], 2])
b.points[:, 0] = x
b.points[:, 1] = y
b.writeToFile(boundFilename)

# create and run simulation 
s = fs.sim2D(width, spacing, density, viscosity, deltaT, errorTol, pseudoPressureIt, realPressureIt, smoothIt, maxIt, initialP, initialV, boundFilename, fluidReadFilename, fluidWriteFilename, executableDir)
s.runSim()
