import fluidSimulation2D as fs
from matplotlib import pyplot as plt
import numpy as np

f = fs.fluid2D()
b = fs.boundary2D()
sp = fs.sim2D()

f.readFromFile('txt/f.txt')
b.readFromFile('txt/b.txt')
sp.readFromFile('simulation_parameters.txt')

Re = f.Re(np.sqrt(sp.initialV[0] ** 2 + sp.initialV[1] ** 2), max(b.points[:, 0] - min(b.points[:, 0])))
Re = round(Re, 2 - int(np.log10(Re)))

f.plotContour(f.p, contourLines=50, title='Pressure Contour with Streamlines for Re = ' + str(Re), filled=True)
f.plotFlow(0.001e-3, 20, 1, 10000, 0)
b.plotBound()
plt.show()
