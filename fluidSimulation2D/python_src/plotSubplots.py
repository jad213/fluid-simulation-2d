import fluidSimulation2D as fs
from matplotlib import pyplot as plt
import numpy as np

f = fs.fluid2D()
b = fs.boundary2D()

f.readFromFile('txt/f.txt')
b.readFromFile('txt/b.txt')

f.plotContourSubplots(b, filled=True, contourLines=100)
f.plotProfileSubplots()
f.plotProfileSubplots(1)
