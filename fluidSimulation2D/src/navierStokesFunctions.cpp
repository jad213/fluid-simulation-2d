#include "fluidSimulation2d.h"

void updateVelocity(fluid2d& f, boundary2d& b, float deltaT)
// updates the velocity via navier stokes equaitons by one timeStep
{
	vectorField2d dvdt = deltaT * ((f.viscosity / f.density) * f.v.laplacian() - f.v.vDotGradV() - 1 / f.density * f.p.grad());

	f.v += dvdt;
}

void solvePoisson(scalarField2d& p, scalarField2d& phi, int iterations, bool setFreeStream, float freeStreamP)
// solves laplacian(p) = phi using iterative method
{
	scalarField2d newP = p;

	const float cornerP = p.points[0][0];
	const float dx2 = p.deltaX * p.deltaX;
	const float dy2 = p.deltaY * p.deltaY;
	const float c1 = 0.5f / (dx2 + dy2);
	const float cpx = dy2 * c1;
	const float cpy = dx2 * c1;
	const float ceta = dx2 * dy2 * c1;

	int ix, iy;

	for (int i = 0; i < iterations; i++)
	{
		// set pressure at each point
		for (ix = 1; ix < p.xPoints - 1; ix++)
		{
			for (iy = 1; iy < p.yPoints - 1; iy++)
			{
				newP.points[ix][iy] = cpx * (p.points[ix + 1][iy] + p.points[ix - 1][iy]) + cpy * (p.points[ix][iy + 1] + p.points[ix][iy - 1]) - ceta * phi.points[ix][iy];
			}
		}

		p = newP;

		// correct edges
		if (setFreeStream)
		{
			for (ix = 0; ix < p.xPoints; ix++)
			{
				p.points[ix][0] = freeStreamP;
				p.points[ix][1] = freeStreamP;
				p.points[ix][p.yPoints - 1] = freeStreamP;
				p.points[ix][p.yPoints - 2] = freeStreamP;
			}
			for (iy = 0; iy < p.yPoints; iy++)
			{
				p.points[0][iy] = freeStreamP;
				p.points[1][iy] = freeStreamP;
				p.points[p.xPoints - 1][iy] = freeStreamP;
				p.points[p.xPoints - 2][iy] = freeStreamP;
			}
		}
		else {
			for (ix = 0; ix < p.xPoints; ix++)
			{
				p.points[ix][0] = p.points[ix][1];
				p.points[ix][p.yPoints - 1] = p.points[ix][p.yPoints - 2];
			}
			for (iy = 0; iy < p.yPoints; iy++)
			{
				p.points[0][iy] = p.points[1][iy];
				p.points[p.xPoints - 1][iy] = p.points[p.xPoints - 2][iy];
			}
		}
	}

	// correct for constant offset
	float offset = p.points[0][0] - cornerP;

	for (ix = 0; ix < p.xPoints - 0; ix++)
	{
		for (iy = 0; iy < p.yPoints - 0; iy++)
		{
			p.points[ix][iy] -= offset;
		}
	}
}

void solvePseudoPressure(fluid2d& f, boundary2d& b, float deltaT, int iterations, float initialP)
{
	index2d p;

	vectorField2d laplace = f.viscosity * f.v.laplacian();
	vectorField2d vDotGradV = f.density * f.v.vDotGradV();
	vectorField2d eta = (f.density / deltaT) * f.v + laplace - vDotGradV;
	scalarField2d newP = f.p;

	scalarField2d divEta = eta.div();
	scalarField2d divEtaBound = (laplace - vDotGradV).div();

	for (unsigned int i = 0; i < b.pointsOnBound.size(); i++)
	{
		p = b.pointsOnBound[i];
		divEta.points[p.x][p.y] = divEtaBound.points[p.x][p.y];
	}
	/*
	for (unsigned int i = 0; i < b.pointsInsideBound.size(); i++)
	{
		p = b.pointsInsideBound[i];
		divEta.points[p.x][p.y] = divEtaBound.points[p.x][p.y];
	}
	*/
	solvePoisson(f.p, divEta, iterations, false, 0.0f);
}

void solveRealPressure(fluid2d& f, int iterations, float initialP)
{
	// only run if iterations > 0
	if (iterations > 0)
	{
		// find eta assuming steady flow, i.e partial dv/dt = 0
		vectorField2d eta = f.viscosity * f.v.laplacian() - f.density * f.v.vDotGradV();
		scalarField2d divEta = eta.div();

		// sovle laplacian(p) = divEta
		solvePoisson(f.p, divEta, iterations, true, initialP);
	}
}

void simulate(fluid2d& f, boundary2d& b, float deltaT, float maxAvError, int pseudoPressureIt, int realPressureIt, int smoothIt, int maxIt, float initialP, vector2d initialV)
{	
	int i, it = 0;

	for (i = 0; i < smoothIt; i++)
	{
		b.zeroFieldOnBound(f.v);
		b.mirrorField(f.v);
		b.smoothVelInsideBound(f);
	}

	cout << "Solve Iteration : " << 0 << endl;
	cout << "Velocity MSE    : " << f.velMSE(b, initialV.abs()) << endl;

	while ((f.velMSE(b, initialV.abs()) >= maxAvError) && (it < maxIt))
	{
		// increment it
		it++;

		// boundary conditions
		for (i = 0; i < smoothIt; i++)
		{
			b.zeroFieldOnBound(f.v);
			b.mirrorField(f.v);
			f.smoothVel();
		}
		b.zeroFieldOnBound(f.v);
		b.mirrorField(f.v);
		f.mirrorEdges();

		if (it % 50 == 0)
		{
			cout << "Solve Iteration : " << it << endl;
			cout << "Velocity MSE    : " << f.velMSE(b, initialV.abs()) << endl;
		}

		solvePseudoPressure(f, b, deltaT, pseudoPressureIt, initialP);

		updateVelocity(f, b, deltaT);
	}

	// boundary conditions
	//b.solveVelInBoundary(f, boundIt);
	b.zeroFieldOnBound(f.v);
	b.mirrorField(f.v);

	solveRealPressure(f, realPressureIt, initialP);
}
