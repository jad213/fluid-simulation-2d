#pragma once

#include "fluidSimulation2d.h"

vectorField2d::vectorField2d()
// default constructor
{
	xPoints = 10;
	yPoints = 10;
	resize(xPoints, yPoints);
}

vectorField2d::vectorField2d(int xPoints, int yPoints)
{
	vectorField2d::xPoints = xPoints; 
	vectorField2d::yPoints = yPoints;
	resize(xPoints, yPoints);
}

vectorField2d::vectorField2d(int xPoints, int yPoints, float deltaX, float deltaY)
// constructor
{
	vectorField2d::xPoints = xPoints;
	vectorField2d::yPoints = yPoints;

	resize(xPoints, yPoints, deltaX, deltaY);
}

vectorField2d::vectorField2d(int xPoints, int yPoints, float deltaX, float deltaY, vector2d initialValue)
// constructor
{
	vectorField2d::xPoints = xPoints; 
	vectorField2d::yPoints = yPoints;

	resize(xPoints, yPoints, deltaX, deltaY);
	uniformField(initialValue);
}

vectorField2d::vectorField2d(int xPoints, int yPoints, float deltaX, float deltaY, vector<vector<vector2d>> points)
{
	vectorField2d::xPoints = xPoints;
	vectorField2d::yPoints = yPoints;
	vectorField2d::deltaX = deltaX;
	vectorField2d::deltaY = deltaY;

	// check points is the correct size
	assert(points.size() == xPoints);
	for (int ix = 0; ix < xPoints; ix++)
	{
		assert(points[ix].size() == yPoints);
	}

	vectorField2d::points = points;
}

vectorField2d::vectorField2d(scalarField2d x, scalarField2d y)
{
	// check fields are compatible
	x.checkSameShape(y);

	deltaX = x.deltaX;
	deltaY = x.deltaY;
	xPoints = x.xPoints;
	yPoints = x.yPoints;

	resize(xPoints, yPoints);

	for (int ix = 0; ix < xPoints; ix++)
	{
		for (int iy = 0; iy < yPoints; iy++)
		{
			points[ix][iy].x = x.points[ix][iy];
			points[ix][iy].y = y.points[ix][iy];
		}
	}
}

// operators
bool vectorField2d::operator== (const vectorField2d& v)
{
	// find && of all these conditions
	bool out = true;
	out = out && (xPoints == v.xPoints);
	out = out && (yPoints == v.yPoints);
	out = out && (deltaX == v.deltaX);
	out = out && (deltaY == v.deltaY);
	out = out && (points == v.points);

	return out;
}

vectorField2d vectorField2d::operator+ (const vectorField2d& v)
{
	// check scalar fields are compatible
	checkSameShape(v);

	// output field
	vectorField2d out(xPoints, yPoints, deltaX, deltaY);

	// iterate over field, adding elements
	for (int ix = 0; ix < xPoints; ix++)
	{
		for (int iy = 0; iy < yPoints; iy++)
		{
			out.points[ix][iy] = points[ix][iy] + v.points[ix][iy];
		}
	}

	return out;
}

vectorField2d vectorField2d::operator- ()
{
	// output field
	vectorField2d out(xPoints, yPoints, deltaX, deltaY);

	// iterate over field, subtracting elements
	for (int ix = 0; ix < xPoints; ix++)
	{
		for (int iy = 0; iy < yPoints; iy++)
		{
			out.points[ix][iy] = -points[ix][iy];
		}
	}

	return out;
}

vectorField2d& vectorField2d::operator+= (const vectorField2d& v)
{
	// check scalar fields are compatible
	checkSameShape(v);

	*this = *this + v;
	return *this;
}

vectorField2d& vectorField2d::operator-= (const vectorField2d& v)
{
	// check scalar fields are compatible
	checkSameShape(v);

	*this = *this - v;
	return *this;
}

vectorField2d vectorField2d::operator- (const vectorField2d& v)
{
	// check scalar fields are compatible
	checkSameShape(v);

	// output field
	vectorField2d out(xPoints, yPoints, deltaX, deltaY);

	// iterate over field, subtracting elements
	for (int ix = 0; ix < xPoints; ix++)
	{
		for (int iy = 0; iy < yPoints; iy++)
		{
			out.points[ix][iy] = points[ix][iy] - v.points[ix][iy];
		}
	}

	return out;
}

scalarField2d vectorField2d::operator* (const vectorField2d& v)
{
	// check scalar fields are compatible
	checkSameShape(v);

	// output field
	scalarField2d out(xPoints, yPoints, deltaX, deltaY);

	// iterate over field, multiplying elements
	for (int ix = 0; ix < xPoints; ix++)
	{
		for (int iy = 0; iy < yPoints; iy++)
		{
			out.points[ix][iy] = points[ix][iy] * v.points[ix][iy];
		}
	}

	return out;
}

vectorField2d operator* (const vectorField2d& v, const float& a)
{
	// output field
	vectorField2d out(v.xPoints, v.yPoints, v.deltaX, v.deltaY);

	// iterate over field, multiplying elements
	for (int ix = 0; ix < v.xPoints; ix++)
	{
		for (int iy = 0; iy < v.yPoints; iy++)
		{
			out.points[ix][iy] = v.points[ix][iy] * a;
		}
	}

	return out;
}

vectorField2d operator* (const float& a, const vectorField2d& v)
{
	// output field
	vectorField2d out(v.xPoints, v.yPoints, v.deltaX, v.deltaY);

	// iterate over field, multiplying elements
	for (int ix = 0; ix < v.xPoints; ix++)
	{
		for (int iy = 0; iy < v.yPoints; iy++)
		{
			out.points[ix][iy] = v.points[ix][iy] * a;
		}
	}

	return out;
}

vectorField2d& vectorField2d::operator*= (const float& a)
{
	*this = *this * a;
	return *this;
}

vectorField2d& vectorField2d::operator/= (const float& a)
{
	*this = *this * (1 / a);
	return *this;
}

ostream& operator<< (ostream& out, const vectorField2d& v) 
{
	for (int iy = 0; iy < v.yPoints; iy++) 
	{
		for (int ix = 0; ix < v.xPoints; ix++) 
		{
			out << "[" << v.points[ix][iy] << "], ";
		}
		out << endl;
	}
	return out;
}

// numerical functions

vectorField2d vectorField2d::xDerivative(void)
{
	// create vectorField2d out to return
	vectorField2d out(xPoints, yPoints, deltaX, deltaY);

	const float eulerFactor = 1 / deltaX;
	const float midpointFactor = 0.5f * eulerFactor;
	const float twelth = 1.0f / 12.0f * eulerFactor;
	const float twoThirds = 2.0f / 3.0f * eulerFactor;

	int ix, iy;

	// middle points
	// iterate over all x except 0th and last point as these cannot be done with midpoint method
	for (ix = 2; ix < xPoints - 2; ix++)
	{
		// iterate over all y points
		for (iy = 0; iy < yPoints; iy++)
		{
			// midpoint method
			out.points[ix][iy] = 
				points[ix + 2][iy] * -twelth + points[ix + 1][iy] *  twoThirds +
				points[ix - 2][iy] *  twelth + points[ix - 1][iy] * -twoThirds;
		}
	}

	// side points using euler method
	// iterate over all y
	for (iy = 0; iy < yPoints; iy++)
	{
		// eulers method
		out.points[0][iy] = (points[1][iy] - points[0][iy]) * eulerFactor;
		out.points[xPoints - 1][iy] = (points[xPoints - 1][iy] - points[xPoints - 2][iy]) * eulerFactor;

		// midpoint method
		out.points[1][iy] = (points[2][iy] - points[0][iy]) * midpointFactor;
		out.points[xPoints - 2][iy] = (points[xPoints - 1][iy] - points[xPoints - 3][iy]) * midpointFactor;
	}

	return out;
}

vectorField2d vectorField2d::yDerivative(void)
{
	// create vectorField2d out to return
	vectorField2d out(xPoints, yPoints, deltaX, deltaY);

	// derivative factors, precaculate to optimize
	const float eulerFactor = 1 / deltaY;
	const float midpointFactor = 0.5f * eulerFactor;
	const float twelth = 1.0f / 12.0f * eulerFactor;
	const float twoThirds = 2.0f / 3.0f * eulerFactor;

	int ix, iy;

	// middle points using midpoint method
	// iterate over all x
	for (ix = 0; ix < xPoints; ix++)
	{
		// iterate over all y except 0th and last point as these cannot be done with midpoint method
		for (iy = 2; iy < yPoints - 2; iy++)
		{
			// midpoint method
			out.points[ix][iy] =
				points[ix][iy + 2] * -twelth + points[ix][iy + 1] *  twoThirds +
				points[ix][iy - 2] *  twelth + points[ix][iy + 1] * -twoThirds;
		}
	}

	// side points using euler method
	// iterate over all x
	for (ix = 0; ix < xPoints; ix++)
	{
		// eulers method
		out.points[ix][0] = (points[ix][1] - points[ix][0]) * eulerFactor;
		out.points[ix][yPoints - 1] = (points[ix][yPoints - 1] - points[ix][yPoints - 2]) * eulerFactor;

		// midpoint method
		out.points[ix][1] = (points[ix][2] - points[ix][0]) * midpointFactor;
		out.points[ix][xPoints - 2] = (points[ix][xPoints - 1] - points[ix][xPoints - 3]) * midpointFactor;
	}

	return out;
}

vectorField2d vectorField2d::xSecondDerivative(void)
{
	// create vectorField2d out to return
	vectorField2d out(xPoints, yPoints, deltaX, deltaY);

	const float centralDiffFactor = 1 / (deltaX * deltaX);

	// middle points
	// iterate over all x except first 0th and last point as these cannot be done with central difference method
	for (int ix = 1; ix < xPoints - 1; ix++)
	{
		// iterate over all y points
		for (int iy = 0; iy < yPoints; iy++)
		{
			// midpoint method
			out.points[ix][iy] = (points[ix + 1][iy] - 2 * points[ix][iy] + points[ix - 1][iy]) * centralDiffFactor;
		}
	}

	// side points setting derivative to the derivative of the adjacent point
	// iterate over all y
	for (int iy = 0; iy < yPoints; iy++)
	{
		// eulers method
		out.points[0][iy] = out.points[1][iy];
		out.points[xPoints - 1][iy] = out.points[xPoints - 2][iy];
	}

	return out;
}

vectorField2d vectorField2d::ySecondDerivative(void)
{
	// create vectorField2d out to return
	vectorField2d out(xPoints, yPoints, deltaX, deltaY);

	const float centralDiffFactor = 1 / (deltaY * deltaY);

	// middle points
	// iterate over all x points
	for (int ix = 0; ix < xPoints; ix++)
	{
		// iterate over all y except first 0th and last point as these cannot be done with central difference method
		for (int iy = 1; iy < yPoints - 1; iy++)
		{
			// midpoint method
			out.points[ix][iy] = (points[ix][iy + 1] - 2 * points[ix][iy] + points[ix][iy - 1]) * centralDiffFactor;
		}
	}

	// side points setting derivative to the derivative of the adjacent point
	// iterate over all x
	for (int ix = 0; ix < xPoints; ix++)
	{
		// eulers method
		out.points[ix][0] = out.points[ix][1];
		out.points[ix][yPoints - 1] = out.points[ix][yPoints - 2];
	}

	return out;
}

scalarField2d vectorField2d::div(void)
{
	// split field into components
	scalarField2d vx(xPoints, yPoints, deltaX, deltaY);
	scalarField2d vy(xPoints, yPoints, deltaX, deltaY);
	components(vx, vy);

	return vx.xDerivative() + vy.yDerivative();
}

vectorField2d vectorField2d::laplacian(void)
{
	return xSecondDerivative() + ySecondDerivative();
}

vectorField2d vectorField2d::vDotGradV(void)
{
	vectorField2d xDeriv = xDerivative();
	vectorField2d yDeriv = yDerivative();

	vectorField2d out(xPoints, yPoints, deltaX, deltaY);

	// iterate over field
	for (int ix = 0; ix < xPoints; ix++)
	{
		for (int iy = 0; iy < yPoints; iy++)
		{
			// set out to vx * dv/dx + vy * dv/dy
			out.points[ix][iy] = xDeriv.points[ix][iy] * points[ix][iy].x + yDeriv.points[ix][iy] * points[ix][iy].y;
		}
	}

	return out;
}

scalarField2d vectorField2d::curl(void)
// returns the component of the curl in the z direction as a scalar
{
	// split field into components
	scalarField2d vx(xPoints, yPoints, deltaX, deltaY);
	scalarField2d vy(xPoints, yPoints, deltaX, deltaY);
	components(vx, vy);
	
	return vy.xDerivative() - vx.yDerivative();
}

// other functions
float vectorField2d::avMag(void)
{
	float out = 0;
	//float divFactor = 1 / (xPoints * yPoints);

	for (int ix = 0; ix < xPoints; ix++)
	{
		for (int iy = 0; iy < yPoints; iy++)
		{
			out += points[ix][iy].abs();
		}
	}
	return out / (xPoints * yPoints);
}

void vectorField2d::uniformField(vector2d v)
// sets all vectors in the vectorField2d to v
{
	for (int ix = 0; ix < xPoints; ix++)
	{
		for (int iy = 0; iy < yPoints; iy++)
		{
			points[ix][iy] = v;
		}
	}
}

scalarField2d vectorField2d::component(int dimension)
{
	scalarField2d out(xPoints, yPoints, deltaX, deltaY);

	for (int ix = 0; ix < xPoints; ix++)
	{
		// iterate over all y points
		for (int iy = 0; iy < yPoints; iy++)
		{
			if (dimension == 0)
			{
				out.points[ix][iy] = points[ix][iy].x;
			}
			else if (dimension == 1)
			{
				out.points[ix][iy] = points[ix][iy].y;
			}
			else 
			{
				cout << "In vectorField2d::component(int dimension), dimension == " << dimension << endl;
				throw invalid_argument("dimension must be x (0) or y (1)");
			}
		}
	}

	return out;
}

void vectorField2d::components(scalarField2d& x, scalarField2d& y)
{
	for (int ix = 0; ix < xPoints; ix++)
	{
		// iterate over all y points
		for (int iy = 0; iy < yPoints; iy++)
		{
			x.points[ix][iy] = points[ix][iy].x;
			y.points[ix][iy] = points[ix][iy].y;
		}
	}
}

void vectorField2d::checkSameShape(const vectorField2d& v)
{
	assert(xPoints == v.xPoints);
	assert(yPoints == v.yPoints);
	assert(deltaX == v.deltaX);
	assert(deltaY == v.deltaY);
}

void vectorField2d::resize(int xPoints, int yPoints)
// resizes the vector field. Only called by constructors
{
	vectorField2d::xPoints = xPoints;
	vectorField2d::yPoints = yPoints;

	points.resize(xPoints);

	for (int i = 0; i < xPoints; i++)
	{
		points[i].resize(yPoints);
	}
}

void vectorField2d::resize(int xPoints, int yPoints, float deltaX, float deltaY)
{
	vectorField2d::deltaX = deltaX;
	vectorField2d::deltaY = deltaY;

	resize(xPoints, yPoints);
}