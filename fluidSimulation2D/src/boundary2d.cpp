#include "fluidSimulation2d.h"

boundary2d::boundary2d()
// default constructor for boundary2d
{
	// set default points as a square
	vector<index2d> a = {
		index2d(10, 10),
		index2d(11, 10),
		index2d(11, 11),
		index2d(10, 11),
		index2d(10, 10)
	};
	points = a;
	minCorner = index2d(10, 10);
	maxCorner = index2d(11, 11);

	deltaX = 1.0f;
	deltaY = 1.0f;

	initBoundary();
}

boundary2d::boundary2d(vector<index2d>& indexPoints, float deltaX, float deltaY)
// argument: index points, a vector of the indecies (ix, iy) of each vertex of the boundary. 
// These are ordered moving anticlockwise around the boundary (boundary should not be self intersecting!)
{
	points = indexPoints;

	if (points.back() != points[0]) points.push_back(points[0]);

	boundary2d::deltaX = deltaX;
	boundary2d::deltaY = deltaY;

	// set the min and max corner for the smallest rectangle wich surrounds the boundary
	// this rectangle is used for optimisation
	minCorner = points[0];
	maxCorner = points[0];
	for (unsigned int i = 0; i < points.size(); i++)
	{
		if (points[i].x < minCorner.x) minCorner.x = points[i].x;
		if (points[i].y < minCorner.y) minCorner.y = points[i].y;
		if (points[i].x > maxCorner.x) maxCorner.x = points[i].x;
		if (points[i].y > maxCorner.y) maxCorner.y = points[i].y;
	}

	initBoundary();
}

boundary2d::boundary2d(vector<vector2d>& vectorPoints, float deltaX, float deltaY)
// arguments: vectorPoints is a vector of the verticies of the boundary in anticlockwise order
// deltaX: x spacing of the simulation coordinate grid
// deltaY: y spacing of the simulation coordinate grid
{
	boundary2d::deltaX = deltaX;
	boundary2d::deltaY = deltaY;

	// convert the vector2d positions into index2d positions by normalising them wrt the coordinate grid
	vector<index2d> pointsNorm;
	pointsNorm.resize(vectorPoints.size());

	for (unsigned int i = 0; i < vectorPoints.size(); i++)
	{
		pointsNorm[i].x = int(round(vectorPoints[i].x / deltaX));
		pointsNorm[i].y = int(round(vectorPoints[i].y / deltaY));
	}

	points = pointsNorm;

	if (points.back() != points[0]) points.push_back(points[0]);

	// set the min and max corner for the smallest rectangle wich surrounds the boundary
	// this rectangle is used for optimisation
	minCorner = points[0];
	maxCorner = points[0];
	for (unsigned int i = 0; i < points.size(); i++)
	{
		if (points[i].x < minCorner.x) minCorner.x = points[i].x;
		if (points[i].y < minCorner.y) minCorner.y = points[i].y;
		if (points[i].x > maxCorner.x) maxCorner.x = points[i].x;
		if (points[i].x > maxCorner.y) maxCorner.y = points[i].y;
	}

	initBoundary();
}

void boundary2d::initBoundary()
// initalizes the boudary by creating the boundary inner layers 1 & 2, and the pointsInsideBoundry vector
{
	int ix, iy, direction, boundPointIndex, maxPoints;
	bool insideBound;
	float dist, mGrad, hypDist;
	index2d p;
	boundInnerPoint boundp;

	// max number of boundary points is just all the points in the rectangle which surrounds the boundary
	maxPoints = (maxCorner.x - minCorner.x) * (maxCorner.y - minCorner.y);

	pointsInsideBound.clear();
	pointsOnBound.clear();
	pointsNearBound.clear();

	// reserve maxPoints amount of memory to avoid memory reallocation
	pointsInsideBound.reserve(maxPoints);
	pointsOnBound.reserve(maxPoints);
	pointsNearBound.reserve(maxPoints);
	
	// iterate over all points that could be inside the boundary, 
	// i.e. are inside the min and max x and y
	for (ix = minCorner.x - 1; ix <= maxCorner.x + 1; ix++)
	{
		for (iy = minCorner.x - 1; iy <= maxCorner.x + 1; iy++)
		{
			p.x = ix;
			p.y = iy;

			insideBound = findMinDistToBoundary(p, dist, direction, boundPointIndex);

			if (insideBound)
			{
				// add point to points inside bound
				pointsInsideBound.push_back(p);

				// point is close the the boundary, so needs velocity mirroring
				// different case depending on how close the point is the the boundary
				if (dist < 0.5)
				{
					boundp.p = p;

					// different case for each direction
					if (direction == 0)
					{
						mGrad = -float(points[boundPointIndex + 1].x - points[boundPointIndex].x) / (points[boundPointIndex + 1].y - points[boundPointIndex].y);
						boundp.mp1 = index2d(p.x + 1, p.y);

						if (mGrad > 0) boundp.mp2 = index2d(p.x + 1, p.y + 1);
						else boundp.mp2 = index2d(p.x + 1, p.y - 1);
					}
					else if (direction == 1)
					{
						mGrad = float(points[boundPointIndex + 1].y - points[boundPointIndex].y) / (points[boundPointIndex + 1].x - points[boundPointIndex].x);
						boundp.mp1 = index2d(p.x, p.y + 1);

						if (mGrad > 0) boundp.mp2 = index2d(p.x - 1, p.y + 1);
						else boundp.mp2 = index2d(p.x + 1, p.y + 1);
					}
					else if (direction == 2)
					{
						mGrad = -float(points[boundPointIndex + 1].x - points[boundPointIndex].x) / (points[boundPointIndex + 1].y - points[boundPointIndex].y);
						boundp.mp1 = index2d(p.x - 1, p.y);

						if (mGrad > 0) boundp.mp2 = index2d(p.x - 1, p.y - 1);
						else boundp.mp2 = index2d(p.x - 1, p.y + 1);
					}
					else
					{
						mGrad = float(points[boundPointIndex + 1].y - points[boundPointIndex].y) / (points[boundPointIndex + 1].x - points[boundPointIndex].x);
						boundp.mp1 = index2d(p.x, p.y - 1);

						if (mGrad > 0) boundp.mp2 = index2d(p.x + 1, p.y - 1);
						else boundp.mp2 = index2d(p.x - 1, p.y - 1);
					}

					boundp.mw = abs(mGrad);
					hypDist = dist / (1 + mGrad * mGrad);
					boundp.w = hypDist / (hypDist - 1);

					pointsNearBound.push_back(boundp);
				}
				else if (dist < 1.5)
				{
					boundp.p = p;

					if (direction == 0)
					{
						mGrad = -float(points[boundPointIndex + 1].x - points[boundPointIndex].x) / (points[boundPointIndex + 1].y - points[boundPointIndex].y);
						boundp.mp1 = index2d(p.x + 2, p.y);

						if (mGrad > 0) boundp.mp2 = index2d(p.x + 2, p.y + 2);
						else boundp.mp2 = index2d(p.x + 2, p.y - 2);
					}
					else if (direction == 1)
					{
						mGrad = float(points[boundPointIndex + 1].y - points[boundPointIndex].y) / (points[boundPointIndex + 1].x - points[boundPointIndex].x);
						boundp.mp1 = index2d(p.x, p.y + 2);

						if (mGrad > 0) boundp.mp2 = index2d(p.x - 2, p.y + 2);
						else boundp.mp2 = index2d(p.x + 2, p.y + 2);
					}
					else if (direction == 2)
					{
						mGrad = -float(points[boundPointIndex + 1].x - points[boundPointIndex].x) / (points[boundPointIndex + 1].y - points[boundPointIndex].y);
						boundp.mp1 = index2d(p.x - 2, p.y);

						if (mGrad > 0) boundp.mp2 = index2d(p.x - 2, p.y - 2);
						else boundp.mp2 = index2d(p.x - 2, p.y + 2);
					}
					else
					{
						mGrad = float(points[boundPointIndex + 1].y - points[boundPointIndex].y) / (points[boundPointIndex + 1].x - points[boundPointIndex].x);
						boundp.mp1 = index2d(p.x, p.y - 2);

						if (mGrad > 0) boundp.mp2 = index2d(p.x + 1, p.y - 1);
						else boundp.mp2 = index2d(p.x - 2, p.y - 2);
					}

					boundp.mw = abs(mGrad);
					hypDist = dist / (1 + mGrad * mGrad);
					boundp.w = hypDist / (hypDist - 2);

					pointsNearBound.push_back(boundp);
				}
			}
			// case for points not inside boundary, but still close to boundary
			else if (dist < TOL) pointsOnBound.push_back(p);
			else if (dist < 0.5)
			{
				boundp.p = p;

				if (direction == 0)
				{
					mGrad = -float(points[boundPointIndex + 1].x - points[boundPointIndex].x) / (points[boundPointIndex + 1].y - points[boundPointIndex].y);
					boundp.mp1 = index2d(p.x - 1, p.y);

					if (mGrad > 0) boundp.mp2 = index2d(p.x - 1, p.y - 1);
					else boundp.mp2 = index2d(p.x - 1, p.y + 1);
				}
				else if (direction == 1)
				{
					mGrad = float(points[boundPointIndex + 1].y - points[boundPointIndex].y) / (points[boundPointIndex + 1].x - points[boundPointIndex].x);
					boundp.mp1 = index2d(p.x, p.y - 1);

					if (mGrad > 0) boundp.mp2 = index2d(p.x + 1, p.y - 1);
					else boundp.mp2 = index2d(p.x - 1, p.y - 1);
				}
				else if (direction == 2)
				{
					mGrad = -float(points[boundPointIndex + 1].x - points[boundPointIndex].x) / (points[boundPointIndex + 1].y - points[boundPointIndex].y);
					boundp.mp1 = index2d(p.x + 1, p.y);

					if (mGrad > 0) boundp.mp2 = index2d(p.x + 1, p.y + 1);
					else boundp.mp2 = index2d(p.x + 1, p.y - 1);
				}
				else
				{
					mGrad = float(points[boundPointIndex + 1].y - points[boundPointIndex].y) / (points[boundPointIndex + 1].x - points[boundPointIndex].x);
					boundp.mp1 = index2d(p.x, p.y + 1);

					if (mGrad > 0) boundp.mp2 = index2d(p.x - 1, p.y + 1);
					else boundp.mp2 = index2d(p.x + 1, p.y + 1);
				}

				boundp.mw = abs(mGrad);
				hypDist = dist / (1 + mGrad * mGrad);
				boundp.w = hypDist / (hypDist + 1);

				pointsNearBound.push_back(boundp);
			}
			else if (dist < 1.5)
			{
				boundp.p = p;

				if (direction == 0)
				{
					mGrad = -float(points[boundPointIndex + 1].x - points[boundPointIndex].x) / (points[boundPointIndex + 1].y - points[boundPointIndex].y);
					boundp.mp1 = index2d(p.x - 2, p.y);

					if (mGrad > 0) boundp.mp2 = index2d(p.x - 2, p.y - 2);
					else boundp.mp2 = index2d(p.x - 2, p.y + 2);
				}
				else if (direction == 1)
				{
					mGrad = float(points[boundPointIndex + 1].y - points[boundPointIndex].y) / (points[boundPointIndex + 1].x - points[boundPointIndex].x);
					boundp.mp1 = index2d(p.x, p.y - 2);

					if (mGrad > 0) boundp.mp2 = index2d(p.x + 2, p.y - 2);
					else boundp.mp2 = index2d(p.x - 2, p.y - 2);
				}
				else if (direction == 2)
				{
					mGrad = -float(points[boundPointIndex + 1].x - points[boundPointIndex].x) / (points[boundPointIndex + 1].y - points[boundPointIndex].y);
					boundp.mp1 = index2d(p.x + 2, p.y);

					if (mGrad > 0) boundp.mp2 = index2d(p.x + 2, p.y + 2);
					else boundp.mp2 = index2d(p.x + 2, p.y - 2);
				}
				else
				{
					mGrad = float(points[boundPointIndex + 1].y - points[boundPointIndex].y) / (points[boundPointIndex + 1].x - points[boundPointIndex].x);
					boundp.mp1 = index2d(p.x, p.y + 2);

					if (mGrad > 0) boundp.mp2 = index2d(p.x - 2, p.y + 2);
					else boundp.mp2 = index2d(p.x + 2, p.y + 2);
				}

				boundp.mw = abs(mGrad);
				hypDist = dist / (1 + mGrad * mGrad);
				boundp.w = hypDist / (hypDist + 2);

				pointsNearBound.push_back(boundp);
			}
		}
	}

	// remove excess reserved memory for these vectors
	pointsInsideBound.shrink_to_fit();
	pointsOnBound.shrink_to_fit();
	pointsNearBound.shrink_to_fit();
}

bool boundary2d::findMinDistToBoundary(index2d p, float& dist, int& direction, int& boundPointIndex)
// returns true if p is strictly inside the boundary and false if p is on or outside the boundary.
// arguments: p is the point which is being tested to see if its in the boundary,
// dist is an output for the min distance to a boundary
// direction is an output for the direction the min dist is in wrt the points. The directions are below
// boundPointIndex is the lower index of the two points which make up the boundary line which p is closest to
// 
// directions: (they are in order of increasing angle in polar coordinates)
// 0 : +ve x
// 1 : +ve y
// 2 : -ve x
// 3 : -ve y
// these directions are also used as the indicies for the intersects, minDist and closestBoundPointIndex vectors in the function
{
	unsigned int i;

	// this method uses ray casting algorithm: https://en.wikipedia.org/wiki/Point_in_polygon
	// it considers a ray from p to infinity in each of the 4 directions 
	// it also caculates the minimu distance to the boundary in each direction
	// it also gives the lower index of the two points that make up the boundary line closest to p.

	// vectors are length 4 and the index corresponeds to the value in that direction
	// i.e. intersects[2] is the no. of intersects in the 2 (-ve x) direction

	// intersects stores the number of times the ray intersects the polygon when drawn from p to infinity in the corresponding direction
	vector<unsigned int> intersects = { 0, 0, 0, 0 };
	// minDist stores the minimum distance from p to the boundary in a given direction
	vector<float> minDist = { float(maxCorner.x - minCorner.x), float(maxCorner.x - minCorner.x), float(maxCorner.y - minCorner.y), float(maxCorner.y - minCorner.y) }; // initalize to some value garunteed to be greater than min dist
	// closestBoundPointIndex stores the lower index of the two points that make up the boundary line closest to p.
	vector<unsigned int> closestBoundPointIndex = { 0, 0, 0, 0 };

	// iterate through points
	for (i = 0; i < points.size() - 1; i++)
	{
		// +ve & -ve x direction
		// check if the line from points[i] -> points[i+1] has the right y positions for an x line intercept
		if ((p.y >= min(points[i].y, points[i + 1].y)) && (p.y < max(points[i].y, points[i + 1].y )))
		{
			// find the x position of the line at y = p.y
			float xLine;

			//xLine = float(points[i].x) + float((points[i + 1].x - points[i].x) * (p.y - points[i].y)) / float((points[i + 1].y - points[i].y));

			// avoid div zero error
			if (abs(points[i + 1].y - points[i].y) > TOL)
			{
				xLine = float(points[i].x) + float((points[i + 1].x - points[i].x) * (p.y - points[i].y)) / float((points[i + 1].y - points[i].y));
			}
			else if (p.x > max(points[i].x, points[i + 1].x)) xLine = float(max(points[i].x, points[i + 1].x));
			else if (p.x < min(points[i].x, points[i + 1].x)) xLine = float(min(points[i].x, points[i + 1].x));
			else xLine = float(p.x);

			// check which side it intersects
			// special case when on boundary
			if (abs(xLine - p.x) < TOL)
			{
				minDist[0] = 0.0f;
				closestBoundPointIndex[0] = i;
				intersects[0] = 0;
				intersects[1] = 0;
				intersects[2] = 0;
				intersects[3] = 0;
				break;
			}
			else if (p.x < xLine)
			{
				// count intersect
				intersects[0]++;

				// check if dist to boundary is a minimum
				if (xLine - p.x < minDist[0])
				{
					// update minDist and closestBoundPointIndex
					minDist[0] = xLine - p.x;
					closestBoundPointIndex[0] = i;
				}
			}
			else
			{
				// count intersect
				intersects[2]++;

				// check if dist to boundary is a minimum
				if (p.x - xLine < minDist[2]) 
				{
					// update minDist and closestBoundPointIndex
					minDist[2] = p.x - xLine;
					closestBoundPointIndex[2] = i;
				}
			}
		}

		// +ve & -ve y direction
		// check if the line from points[i] -> points[i+1] has the right x positions for a y line intercept
		if ((p.x >= min(points[i].x, points[i + 1].x)) && (p.x < max(points[i].x, points[i + 1].x)))
		{
			// find the x position of the line at y = p.y
			float yLine;

			//yLine = float(points[i].y) + float((points[i + 1].y - points[i].y) * (p.x - points[i].x)) / float(points[i + 1].x - points[i].x);

			// avoid div zero error
			if (abs(points[i + 1].x - points[i].x) > TOL)
			{
				yLine = float(points[i].y) + float((points[i + 1].y - points[i].y) * (p.x - points[i].x)) / float(points[i + 1].x - points[i].x);
			}
			else if (p.y > max(points[i].y, points[i + 1].y)) yLine = float(max(points[i].y, points[i + 1].y));
			else if (p.y < min(points[i].y, points[i + 1].y)) yLine = float(min(points[i].y, points[i + 1].y));
			else yLine = float(p.y);

			//cout << endl;
			//cout << "position    : " << p << endl;
			//cout << "bound points: " << points[i] << "    " << points[i + 1] << endl;
			//cout << "yLine       : " << yLine << endl;

			// check which side it intersects and increment intersections
			// special case when on boundary
			if (abs(yLine - p.y) < TOL)
			{
				minDist[1] = 0.0f;
				closestBoundPointIndex[1] = i;
				intersects[0] = 0;
				intersects[1] = 0;
				intersects[2] = 0;
				intersects[3] = 0;
				break;
			}
			else if (p.y < yLine)
			{
				// count intersect
				intersects[1]++;

				// check if dist to boundary is a minimum
				if (yLine - p.y < minDist[1])
				{
					// update minDist and closestBoundPointIndex
					minDist[1] = yLine - p.y;
					closestBoundPointIndex[1] = i;
				}
			}
			else
			{
				// count intersect
				intersects[3]++;

				// check if dist to boundary is a minimum
				if (p.y - yLine < minDist[3])
				{
					// update minDist and closestBoundPointIndex
					minDist[3] = p.y - yLine;
					closestBoundPointIndex[3] = i;
				}
			}
		}
	}

	// find min dist and direction of min dist
	if (min(minDist[0], minDist[1]) < min(minDist[2], minDist[3]))
	{
		if (minDist[0] < minDist[1])
		{
			dist = minDist[0];
			direction = 0;
			boundPointIndex = closestBoundPointIndex[0];
		}
		else
		{
			dist = minDist[1];
			direction = 1;
			boundPointIndex = closestBoundPointIndex[1];
		}
	}
	else
	{
		if (minDist[2] < minDist[3])
		{
			dist = minDist[2];
			direction = 2;
			boundPointIndex = closestBoundPointIndex[2];
		}
		else
		{
			dist = minDist[3];
			direction = 3;
			boundPointIndex = closestBoundPointIndex[3];
		}
	}



	// check if inside or outside the polygone
	if (intersects[0] % 2 == 0)
	{
		// check the algorithm has worked correctly as these should all be even (point not inside polygon)
		assert(intersects[1] % 2 == 0);
		assert(intersects[2] % 2 == 0);
		assert(intersects[3] % 2 == 0);

		// point is not inside polygon, so return false
		return false;
	}
	// for close points return false as they are on the boundary
	else if (dist < TOL) return false;
	else
	{
		// check the algorithm has worked correctly as these should all be odd (point inside polygon)
		assert(intersects[0] % 2 == 1);
		assert(intersects[1] % 2 == 1);
		assert(intersects[2] % 2 == 1);
		assert(intersects[3] % 2 == 1);

		// point is inside polygon, so return true
		return true;
	}
}

void boundary2d::mirrorField(vectorField2d& v)
{
	unsigned int i;

	for (i = 0; i < pointsNearBound.size(); i++)
	{
		pointsNearBound[i].mirrorBoundPoint(v);
	}
}

void boundary2d::zeroFieldOnBound(vectorField2d& v)
{
	unsigned int i;

	for (i = 0; i < pointsOnBound.size(); i++)
	{
		v.points[pointsOnBound[i].x][pointsOnBound[i].y] = vector2d(0.0f, 0.0f);
	}
}

void boundary2d::zeroFieldInsideBound(vectorField2d& v)
{
	unsigned int i;

	for (i = 0; i < pointsInsideBound.size(); i++)
	{
		v.points[pointsInsideBound[i].x][pointsInsideBound[i].y] = vector2d(0.0f, 0.0f);
	}
}

void boundary2d::smoothVelInsideBound(fluid2d& f)
{
	int ix, iy;
	unsigned int i;

	vectorField2d vOut = f.v;

	for (i = 0; i < pointsInsideBound.size(); i++)
	{
		ix = pointsInsideBound[i].x;
		iy = pointsInsideBound[i].y;
		vOut.points[ix][iy] =
			0.2f * f.v.points[ix][iy] +
			0.2f * f.v.points[ix + 1][iy] +
			0.2f * f.v.points[ix - 1][iy] +
			0.2f * f.v.points[ix][iy + 1] +
			0.2f * f.v.points[ix][iy - 1];
	}

	f.v = vOut;
}

void boundary2d::writeToFile(string filename)
{
	ofstream file;
	file.open(filename);

	for (unsigned int i = 0; i < points.size(); i++)
	{
		file << points[i].x * deltaX << " " << points[i].y * deltaY << endl;		
	}
	file.close();
}

void boundary2d::readFromFile(string filename)
{
	ifstream file;
	file.open(filename);

	string line, character;
	int lineNum;
	index2d point;

	points.clear();

	for (lineNum = 0; getline(file, line, '\n'); lineNum++)
	{
		stringstream fileLine;

		fileLine.str(line);

		getline(fileLine, character, ' ');
		point.x = int(round(stof(character) / deltaX));
		getline(fileLine, character, '\n');
		point.y = int(round(stof(character) / deltaY));

		points.push_back(point);
	}

	if (points[0] != points.back()) points.push_back(points[0]);

	minCorner = points[0];
	maxCorner = points[0];
	for (unsigned int i = 0; i < points.size(); i++)
	{
		if (points[i].x < minCorner.x) minCorner.x = points[i].x;
		if (points[i].y < minCorner.y) minCorner.y = points[i].y;
		if (points[i].x > maxCorner.x) maxCorner.x = points[i].x;
		if (points[i].y > maxCorner.y) maxCorner.y = points[i].y;
	}

	file.close();

	initBoundary();
}
