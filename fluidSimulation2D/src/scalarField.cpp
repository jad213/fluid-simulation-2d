#include "fluidSimulation2d.h"

// constructors
scalarField2d::scalarField2d()
// default constructor
{
	xPoints = 1;
	yPoints = 1;
	deltaX = 1.0f;
	deltaY = 1.0f;
	//resize(xPoints, yPoints);
}

scalarField2d::scalarField2d(int xPoints, int yPoints)
{
	scalarField2d::xPoints = xPoints;
	scalarField2d::yPoints = yPoints;
	resize(xPoints, yPoints);
}

scalarField2d::scalarField2d(int xPoints, int yPoints, float deltaX, float deltaY)
{
	scalarField2d::xPoints = xPoints;
	scalarField2d::yPoints = yPoints;

	resize(xPoints, yPoints, deltaX, deltaY);
}

scalarField2d::scalarField2d(int xPoints, int yPoints, float deltaX, float deltaY, float initialValue)
// constructor
{
	scalarField2d::xPoints = xPoints;
	scalarField2d::yPoints = yPoints;

	resize(xPoints, yPoints, deltaX, deltaY);
	uniformField(initialValue);
}

scalarField2d::scalarField2d(int xPoints, int yPoints, float deltaX, float deltaY, vector<vector<float>> points)
{
	scalarField2d::xPoints = xPoints;
	scalarField2d::yPoints = yPoints;
	scalarField2d::deltaX = deltaX;
	scalarField2d::deltaY = deltaY;

	// check points is the correct size
	assert(points.size() == xPoints);
	for (int ix = 0; ix < xPoints; ix++)
	{
		assert(points[ix].size() == yPoints);
	}

	scalarField2d::points = points;
}

// operators
bool scalarField2d::operator== (const scalarField2d& s) 
{
	// find && of all these conditions
	bool out = true;
	out = out && (xPoints == s.xPoints);
	out = out && (yPoints == s.yPoints);
	out = out && (deltaX == s.deltaX);
	out = out && (deltaY == s.deltaY);
	out = out && (points == s.points);

	return out;
}

scalarField2d scalarField2d::operator+ (const scalarField2d& s)
{
	// check scalar fields are compatible
	checkSameShape(s);
	
	// output field
	scalarField2d out(xPoints, yPoints, deltaX, deltaY);

	// iterate over field, adding elements
	for (int ix = 0; ix < xPoints; ix++)
	{
		for (int iy = 0; iy < yPoints; iy++)
		{
			out.points[ix][iy] = points[ix][iy] + s.points[ix][iy];
		}
	}

	return out;
}

scalarField2d scalarField2d::operator- ()
{
	// output field
	scalarField2d out(xPoints, yPoints, deltaX, deltaY);

	// iterate over field, subtracting elements
	for (int ix = 0; ix < xPoints; ix++)
	{
		for (int iy = 0; iy < yPoints; iy++)
		{
			out.points[ix][iy] = -points[ix][iy];
		}
	}

	return out;
}

scalarField2d& scalarField2d::operator+= (const scalarField2d& s)
{ 
	// check scalar fields are compatible
	checkSameShape(s);

	*this = *this + s;
	return *this; 
}

scalarField2d& scalarField2d::operator-= (const scalarField2d& s)
{
	// check scalar fields are compatible
	checkSameShape(s);

	*this = *this - s;
	return *this;
}

scalarField2d scalarField2d::operator- (const scalarField2d& s)
{
	// check scalar fields are compatible
	checkSameShape(s);

	// output field
	scalarField2d out(xPoints, yPoints, deltaX, deltaY);

	// iterate over field, subtracting elements
	for (int ix = 0; ix < xPoints; ix++)
	{
		for (int iy = 0; iy < yPoints; iy++)
		{
			out.points[ix][iy] = points[ix][iy] - s.points[ix][iy];
		}
	}

	return out;
}

scalarField2d scalarField2d::operator* (const scalarField2d& s)
{
	// check scalar fields are compatible
	checkSameShape(s);

	// output field
	scalarField2d out(xPoints, yPoints, deltaX, deltaY);

	// iterate over field, multiplying elements
	for (int ix = 0; ix < xPoints; ix++)
	{
		for (int iy = 0; iy < yPoints; iy++)
		{
			out.points[ix][iy] = points[ix][iy] * s.points[ix][iy];
		}
	}

	return out;
}

scalarField2d operator* (const scalarField2d& s, const float& a)
{
	// output field
	scalarField2d out(s.xPoints, s.yPoints, s.deltaX, s.deltaY);

	// iterate over field, multiplying elements
	for (int ix = 0; ix < s.xPoints; ix++)
	{
		for (int iy = 0; iy < s.yPoints; iy++)
		{
			out.points[ix][iy] = s.points[ix][iy] * a;
		}
	}

	return out;
}

scalarField2d operator* (const float& a, const scalarField2d& s)
{
	// output field
	scalarField2d out(s.xPoints, s.yPoints, s.deltaX, s.deltaY);

	// iterate over field, multiplying elements
	for (int ix = 0; ix < s.xPoints; ix++)
	{
		for (int iy = 0; iy < s.yPoints; iy++)
		{
			out.points[ix][iy] = s.points[ix][iy] * a;
		}
	}

	return out;
}

scalarField2d& scalarField2d::operator*= (const float& a)
{
	*this = *this * a;
	return *this;
}

scalarField2d& scalarField2d::operator*= (const scalarField2d& s)
{
	*this = *this * s;
	return *this;
}

scalarField2d& scalarField2d::operator/= (const float& a)
{
	*this = *this * (1 / a);
	return *this;
}

ostream& operator<< (ostream& out, const scalarField2d& v) 
{
	for (int iy = 0; iy < v.yPoints; iy++) 
	{
		for (int ix = 0; ix < v.xPoints; ix++) 
		{
			out << v.points[ix][iy] << ", ";
		}
		out << endl;
	}
	return out;
}

// numerical functions
scalarField2d scalarField2d::xDerivative(void)
{
	// create vectorField2d out to return
	scalarField2d out(xPoints, yPoints, deltaX, deltaY);

	const float eulerFactor = 1.0f / deltaX;
	const float midpointFactor = 0.5f * eulerFactor;
	const float twelth = 1.0f / 12.0f * eulerFactor;
	const float twoThirds = 2.0f / 3.0f * eulerFactor;

	int ix, iy;

	// middle points
	// iterate over all x except first 0th and last point as these cannot be done with midpoint method
	for (ix = 2; ix < xPoints - 2; ix++)
	{
		// iterate over all y points
		for (iy = 0; iy < yPoints; iy++)
		{
			// midpoint method
			out.points[ix][iy] =
				points[ix + 2][iy] * -twelth + points[ix + 1][iy] *  twoThirds +
				points[ix - 2][iy] *  twelth + points[ix - 1][iy] * -twoThirds;
		}
	}

	// side points using euler method
	// iterate over all y

	for (int iy = 0; iy < yPoints; iy++)
	{
		// eulers method
		out.points[0][iy] = (points[1][iy] - points[0][iy]) * eulerFactor;
		out.points[xPoints - 1][iy] = (points[xPoints - 1][iy] - points[xPoints - 2][iy]) * eulerFactor;

		// midpoint method
		out.points[1][iy] = (points[2][iy] - points[0][iy]) * midpointFactor;
		out.points[xPoints - 2][iy] = (points[xPoints - 1][iy] - points[xPoints - 3][iy]) * midpointFactor;
	}



	return out;
}

scalarField2d scalarField2d::yDerivative(void)
{
	// create vectorField2d out to return
	scalarField2d out(xPoints, yPoints, deltaX, deltaY);

	// derivative factors, precaculate to optimize
	const float eulerFactor = 1 / deltaY;
	const float midpointFactor = 0.5f * eulerFactor;
	const float twelth = 1.0f / 12.0f * eulerFactor;
	const float twoThirds = 2.0f / 3.0f * eulerFactor;

	int ix, iy;

	// middle points using midpoint method
	// iterate over all x
	for (ix = 0; ix < xPoints; ix++)
	{
		// iterate over all y except first 0th and last point as these cannot be done with midpoint method
		for (iy = 2; iy < yPoints - 2; iy++)
		{
			// midpoint method
			out.points[ix][iy] =
				points[ix][iy + 2] * -twelth + points[ix][iy + 1] * twoThirds +
				points[ix][iy - 2] *  twelth + points[ix][iy - 1] * -twoThirds;
		}
	}

	// side points using euler method
	// iterate over all x
	for (ix = 0; ix < xPoints; ix++)
	{
		// eulers method
		out.points[ix][0] = (points[ix][1] - points[ix][0]) * eulerFactor;
		out.points[ix][yPoints - 1] = (points[ix][yPoints - 1] - points[ix][yPoints - 2]) * eulerFactor;

		// midpoint method
		out.points[ix][1] = (points[ix][2] - points[ix][0]) * midpointFactor;
		out.points[ix][yPoints - 2] = (points[ix][yPoints - 1] - points[ix][yPoints - 3]) * midpointFactor;
	}

	return out;
}

scalarField2d scalarField2d::xSecondDerivative(void)
{
	// create vectorField2d out to return
	scalarField2d out(xPoints, yPoints, deltaX, deltaY);

	const float centralDiffFactor = 1 / (deltaX * deltaX);

	// middle points
	// iterate over all x except first 0th and last point as these cannot be done with central difference method
	for (int ix = 1; ix < xPoints - 1; ix++)
	{
		// iterate over all y points
		for (int iy = 0; iy < yPoints; iy++)
		{
			// midpoint method
			out.points[ix][iy] = (points[ix + 1][iy] - 2 * points[ix][iy] + points[ix - 1][iy]) * centralDiffFactor;
		}
	}

	// side points setting derivative to the derivative of the adjacent point
	// iterate over all y
	for (int iy = 0; iy < yPoints; iy++)
	{
		// eulers method
		out.points[0][iy] = out.points[1][iy];
		out.points[xPoints - 1][iy] = out.points[xPoints - 2][iy];
	}

	return out;
}

scalarField2d scalarField2d::ySecondDerivative(void)
{
	// create vectorField2d out to return
	scalarField2d out(xPoints, yPoints, deltaX, deltaY);

	const float centralDiffFactor = 1 / (deltaY * deltaY);

	// middle points
	// iterate over all x points
	for (int ix = 0; ix < xPoints; ix++)
	{
		// iterate over all y except first 0th and last point as these cannot be done with central difference method
		for (int iy = 1; iy < yPoints - 1; iy++)
		{
			// midpoint method
			out.points[ix][iy] = (points[ix][iy + 1] - 2 * points[ix][iy] + points[ix][iy - 1]) * centralDiffFactor;
		}
	}

	// side points setting derivative to the derivative of the adjacent point
	// iterate over all x
	for (int ix = 0; ix < xPoints; ix++)
	{
		// eulers method
		out.points[ix][0] = out.points[ix][1];
		out.points[ix][yPoints - 1] = out.points[ix][yPoints - 2];
	}

	return out;
}

vectorField2d scalarField2d::grad(void)
{
	return vectorField2d(xDerivative(), yDerivative());
}

scalarField2d scalarField2d::laplacian(void)
{
	return xSecondDerivative() + ySecondDerivative();
}

// other functions
float scalarField2d::avMag(void)
{
	float out = 0;
	//float divFactor = 1 / (xPoints * yPoints);
	
	for (int ix = 0; ix < xPoints; ix++)
	{
		for (int iy = 0; iy < yPoints; iy++)
		{
			out += abs(points[ix][iy]);
		}
	}

	return out / (xPoints * yPoints);
}

void scalarField2d::uniformField(float v)
// sets all vectors in the vectorField2d to v
{
	for (int ix = 0; ix < xPoints; ix++)
	{
		for (int iy = 0; iy < yPoints; iy++)
		{
			points[ix][iy] = v;
		}
	}
}

void scalarField2d::checkSameShape(const scalarField2d& s)
{
	assert(xPoints == s.xPoints);
	assert(yPoints == s.yPoints);
	assert(deltaX == s.deltaX);
	assert(deltaY == s.deltaY);
}

void scalarField2d::resize(int xPoints, int yPoints)
// resizes the vector field. Only called by constructors
{
	scalarField2d::xPoints = xPoints;
	scalarField2d::yPoints = yPoints;

	points.resize(xPoints);

	for (int i = 0; i < xPoints; i++)
	{
		points[i].resize(yPoints);
	}
}

void scalarField2d::resize(int xPoints, int yPoints, float deltaX, float deltaY)
{
	scalarField2d::deltaX = deltaX;
	scalarField2d::deltaY = deltaY;

	resize(xPoints, yPoints);
}