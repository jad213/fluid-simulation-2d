#pragma once

#include "fluidSimulation2d.h"

fluid2d::fluid2d()
{
	xPoints = 1; yPoints = 1;
	deltaX = 1.0f; deltaY = 1.0f;
	density = 1000.0f;
	viscosity = 0.0f;

	//resize(1, 1);
}

fluid2d::fluid2d(int xPoints, int yPoints, float deltaX, float deltaY, float density, float viscosity)
{
	fluid2d::xPoints = xPoints; 
	fluid2d::yPoints = yPoints;
	fluid2d::deltaX = deltaX; 
	fluid2d::deltaY = deltaY;
	fluid2d::density = density;
	fluid2d::viscosity = viscosity;

	resize(xPoints, yPoints, deltaX, deltaY);

	p.uniformField(0.0);
	v.uniformField(vector2d(0.0, 0.0));
}

fluid2d::fluid2d(int xPoints, int yPoints, float deltaX, float deltaY, float density, float viscosity, float initialP, vector2d initialV)
{
	fluid2d::xPoints = xPoints;
	fluid2d::yPoints = yPoints;
	fluid2d::deltaX = deltaX;
	fluid2d::deltaY = deltaY;
	fluid2d::density = density;
	fluid2d::viscosity = viscosity;

	resize(xPoints, yPoints, deltaX, deltaY);
	p.uniformField(initialP);
	v.uniformField(initialV);
}

fluid2d::fluid2d(simParam sp)
{
	xPoints = sp.xPoints;
	yPoints = sp.yPoints;
	deltaX = sp.deltaX;
	deltaY = sp.deltaY;
	density = sp.density;
	viscosity = sp.viscosity;

	resize(xPoints, yPoints, deltaX, deltaY);
	p.uniformField(sp.initialP);
	v.uniformField(sp.initialV);
}

// functions
void fluid2d::smoothVel(void)
{
	int ix, iy;

	vectorField2d vOut = v;

	for (iy = 1; iy < yPoints - 1; iy++)
	{
		for (ix = 1; ix < xPoints - 1; ix++)
		{
			vOut.points[ix][iy] =
				0.2f * v.points[ix][iy] +
				0.2f * v.points[ix + 1][iy] +
				0.2f * v.points[ix - 1][iy] +
				0.2f * v.points[ix][iy + 1] +
				0.2f * v.points[ix][iy - 1];
		}
	}

	v = vOut;
}

void fluid2d::mirrorEdges(void)
{
	// correct edges
	for (int ix = 0; ix < xPoints; ix++)
	{
		v.points[ix][0] = v.points[ix][2];
		v.points[ix][yPoints - 1] = v.points[ix][yPoints - 3];

		v.points[ix][1] = v.points[ix][2];
		v.points[ix][yPoints - 2] = v.points[ix][yPoints - 3];
	}
	for (int iy = 0; iy < yPoints; iy++)
	{
		v.points[0][iy] = v.points[2][iy];
		v.points[xPoints - 1][iy] = v.points[xPoints - 3][iy];

		v.points[1][iy] = v.points[2][iy];
		v.points[xPoints - 2][iy] = v.points[xPoints - 3][iy];
	}
}

void fluid2d::writeToFile(string filename)
{
	ofstream file;
	file.open(filename);

	int ix, iy;

	// write attributes to file
	file << xPoints << endl;
	file << yPoints << endl;
	file << deltaX << endl;
	file << deltaY << endl;
	file << density << endl;
	file << viscosity << endl;

	// write p to file
	for (iy = 0; iy < yPoints; iy++)
	{
		for (ix = 0; ix < xPoints; ix++)
		{
			file << p.points[ix][iy] << ' ';
		}
		file << endl;
	}

	// write v to file
	for (iy = 0; iy < yPoints; iy++)
	{
		for (ix = 0; ix < xPoints; ix++)
		{
			file << '[' << v.points[ix][iy].x << ',' << v.points[ix][iy].y << "] ";
		}
		file << endl;
	}

	file.close();
}

void fluid2d::readFromFile(string filename)
{
	ifstream file;
	file.open(filename);

	string line, character;
	int lineNum, floatNum;
	for (lineNum = 0; getline(file, line, '\n'); lineNum++)
	{	
		if (lineNum == 0) xPoints = stoi(line);
		else if (lineNum == 1) yPoints = stoi(line);
		else if (lineNum == 2) deltaX = stof(line);
		else if (lineNum == 3)
		{
			deltaY = stof(line);
			//resize now we know xPoints and yPoints
			resize(xPoints, yPoints, deltaX, deltaY);
		}
		else if (lineNum == 4) density = stof(line);
		else if (lineNum == 5) viscosity = stof(line);
		else if ((lineNum > 5) && (lineNum < 6 + yPoints))
		{
			// create stringstream to store this line of the file in,
			// then we can iterate through this stringstream
			stringstream fileLine;
			fileLine.str("");
			fileLine << line;

			// iterate through the fileLine
			for (floatNum = 0; getline(fileLine, character, ' '); floatNum++)
			{
				p.points[floatNum][lineNum - 6] = stof(character);
			}
		}
		else if ((lineNum > 5 + yPoints) && (lineNum < 6 + 2 * yPoints))
		{
			// clear fileLine
			stringstream fileLine;
			fileLine.str("");
			fileLine << line;

			// iterate through the fileLine
			for (floatNum = 0; getline(fileLine, character, ' '); floatNum++)
			{
				character >> v.points[floatNum][lineNum - 6 - yPoints];
			}
		}
	}
	file.close();
}

float fluid2d::velMSE(boundary2d b, float freeVelMag)
{
	int ix, iy;

	vectorField2d v0 = v;
	b.zeroFieldInsideBound(v0);

	scalarField2d divV0 = v0.div();

	float errorSum = 0, deltaS = sqrt(deltaX * deltaX + deltaY * deltaY);

	for (ix = 0; ix < xPoints; ix++)
	{
		for (iy = 0; iy < yPoints; iy++)
		{
			errorSum += pow(divV0.points[ix][iy] * deltaS / freeVelMag, 2);
		}
	}

	return errorSum / (xPoints * yPoints);
}

// private functions
void fluid2d::resize(int xPoints, int yPoints, float deltaX, float deltaY)
{
	fluid2d::xPoints = xPoints;
	fluid2d::yPoints = yPoints;

	p.resize(xPoints, yPoints, deltaX, deltaY);
	v.resize(xPoints, yPoints, deltaX, deltaY);
}