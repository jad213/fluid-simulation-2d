// Fluid simulation 2d
// Header file
// James de Salis Young

#pragma once

#define _USE_MATH_DEFINES

#include <iostream>
#include <sstream>
#include <vector>
#include <assert.h>
#include <fstream>
#include <string>
#include <cmath>

#define TOL 1e-5f // tolerance for equality between floats

using namespace std;

// classes
class vector2d;
class index2d;
class scalarField2d;
class vectorField2d;
class fluid2d;
class boundInnerPoint;
class boundary2d;
class simParam;

class vector2d
{
public:
	vector2d() { x = 0.0; y = 0.0; }
	vector2d(float a, float b) { x = a; y = b; }
	bool operator== (const vector2d& v) const { return ((x == v.x) && (y == v.y)); }
	bool operator!= (const vector2d& v) const { return ((x != v.x) || (y != v.y)); }
	vector2d operator+ (const vector2d& v) const { return vector2d(x + v.x, y + v.y); }
	vector2d operator- (const vector2d& v) const { return vector2d(x - v.x, y - v.y); }
	friend vector2d operator- (const vector2d& v) { return vector2d(-v.x, -v.y); }
	vector2d& operator+= (const vector2d& v) { x += v.x; y += v.y; return *this; }
	vector2d& operator-= (const vector2d& v) { x -= v.x; y -= v.y; return *this; }
	float operator* (const vector2d& v) const { return (x * v.x + y * v.y); }
	friend vector2d operator* (const vector2d& v, const float& a) { return vector2d(v.x * a, v.y * a); }
	friend vector2d operator* (const float& a, const vector2d& v) { return vector2d(v.x * a, v.y * a); }
	vector2d& operator*= (const float& a) { x *= a; y *= a; return *this; }
	vector2d operator/ (const float& a) const { return vector2d(x / a, y / a); }
	vector2d& operator/= (const float& a) { x /= a; y /= a; return *this; }
	float abs2() const { return (x * x + y * y); }
	float abs() const { return sqrt(this->abs2()); }
	vector2d norm() const { float s(this->abs()); if (s == 0) return *this; else return vector2d(x / s, y / s); }
	friend ostream& operator<< (ostream& out, const vector2d& v) { out << v.x << ' ' << v.y; return out; }
	friend void operator>> (string s, vector2d& v) 
	{
		stringstream vecSS;
		string temp;
		vecSS << s;
		getline(vecSS, temp, '[');
		getline(vecSS, temp, ',');
		v.x = stof(temp);
		getline(vecSS, temp, ']');
		v.y = stof(temp);
	}
	float x, y;
private:
};

class index2d
{
public:
	// consturctors
	index2d() { x = 0; y = 0; }
	index2d(int x, int y) { index2d::x = x; index2d::y = y; }

	// operators
	bool operator== (const index2d& i) const { return ((x == i.x) && (y == i.y)); }
	bool operator!= (const index2d& i) const { return ((x != i.x) || (y != i.y)); }

	friend ostream& operator<< (ostream& out, const index2d& i) { out << i.x << ' ' << i.y; return out; }

	// atributes
	int x, y;
};

class scalarField2d
{
public:
	// constructors
	scalarField2d();
	scalarField2d(int xPoints, int yPoints);
	scalarField2d(int xPoints, int yPoints, float deltaX, float deltaY);
	scalarField2d(int xPoints, int yPoints, float deltaX, float deltaY, float initialValue);
	scalarField2d(int xPoints, int yPoints, float deltaX, float deltaY, vector<vector<float>> points);
	// operators
	bool operator== (const scalarField2d& s);
	scalarField2d operator+ (const scalarField2d& s);
	scalarField2d operator- ();
	scalarField2d& operator+= (const scalarField2d& s);
	scalarField2d & operator-= (const scalarField2d & s);
	scalarField2d operator- (const scalarField2d& s);
	scalarField2d operator* (const scalarField2d& s);
	friend scalarField2d operator* (const scalarField2d& s, const float& a);
	friend scalarField2d operator* (const float& a, const scalarField2d& s);
	scalarField2d& operator*= (const float& a);
	scalarField2d& operator*= (const scalarField2d& s);
	scalarField2d operator/ (const float& a) { return (*this) * (1 / a); }
	scalarField2d& operator/= (const float& a);
	friend ostream& operator<< (ostream& out, const scalarField2d& s);

	// numerical functions
	scalarField2d xDerivative(void);
	scalarField2d yDerivative(void);
	scalarField2d xSecondDerivative(void);
	scalarField2d ySecondDerivative(void);
	vectorField2d grad(void);
	scalarField2d laplacian(void);

	// other functions
	float avMag(void);
	void uniformField(float s);
	void checkSameShape(const scalarField2d& s);
	void resize(int xPoints, int yPoints);
	void resize(int xPoints, int yPoints, float deltaX, float deltaY);

	// atributes
	int xPoints, yPoints;
	float deltaX, deltaY;
	std::vector<std::vector<float>> points;
};

class vectorField2d
{
public:
	// constructors
	vectorField2d();
	vectorField2d(int xPoints, int yPoints);
	vectorField2d(int xPoints, int yPoints, float deltaX, float deltaY);
	vectorField2d(int xPoints, int yPoints, float deltaX, float deltaY, vector2d initialValue);
	vectorField2d(int xPoints, int yPoints, float deltaX, float deltaY, vector<vector<vector2d>> points);
	vectorField2d(scalarField2d x, scalarField2d y);
	// operators
	bool operator== (const vectorField2d& v);
	vectorField2d operator+ (const vectorField2d& v);
	vectorField2d operator- ();
	vectorField2d& operator+= (const vectorField2d& v);
	vectorField2d& operator-= (const vectorField2d& v);
	vectorField2d operator- (const vectorField2d& v);
	scalarField2d operator* (const vectorField2d& v); // dot product
	friend vectorField2d operator* (const vectorField2d& v, const float& a);
	friend vectorField2d operator* (const float& a, const vectorField2d& v);
	vectorField2d& operator*= (const float& a);
	vectorField2d operator/ (const float& a) { return (*this) * (1 / a); }
	vectorField2d& operator/= (const float& a);
	friend ostream& operator<< (ostream& out, const vectorField2d& v);

	// numerical functions
	vectorField2d xDerivative(void);
	vectorField2d yDerivative(void);
	vectorField2d xSecondDerivative(void);
	vectorField2d ySecondDerivative(void);
	scalarField2d div(void);
	vectorField2d laplacian(void);
	vectorField2d vDotGradV(void);
	scalarField2d curl(void);

	// other functions
	float avMag(void);
	void uniformField(vector2d v);
	scalarField2d component(int dimension);
	void components(scalarField2d& x, scalarField2d& y);
	void checkSameShape(const vectorField2d& v);
	void resize(int xPoints, int yPoints);
	void resize(int xPoints, int yPoints, float deltaX, float deltaY);

	// atributes
	int xPoints, yPoints;
	float deltaX, deltaY;
	std::vector<std::vector<vector2d>> points;	
};

class fluid2d
{
public:
	// constructors
	fluid2d();
	fluid2d(int xPoints, int yPoints, float deltaX, float deltaY, float density, float viscosity);
	fluid2d(int xPoints, int yPoints, float deltaX, float deltaY, float density, float viscosity, float initialP, vector2d initialV);
	fluid2d(simParam sp);

	// functions
	void smoothVel();
	void mirrorEdges();
	void writeToFile(string filename);
	void readFromFile(string filename);
	float velMSE(boundary2d b, float freeVelMag);

	int xPoints, yPoints;
	float deltaX, deltaY, density, viscosity;
	scalarField2d p;
	vectorField2d v;

private:
	void resize(int xPoints, int yPoints, float deltaX, float deltaY);
};

class boundInnerPoint
{
public:
	// constructors
	boundInnerPoint();
	boundInnerPoint(index2d p, index2d mp1, index2d mp2, float w, float mw);

	// functions 
	void mirrorBoundPoint(vectorField2d& velField);

	// atributes
	index2d p, mp1, mp2;
	float w, mw;
};

class boundary2d
{
public:
	// constructors
	boundary2d();
	boundary2d(vector<index2d>& points, float deltaX, float deltaY);
	boundary2d(vector<vector2d>& points, float deltaX, float deltaY);
	
	// functions
	void initBoundary();
	bool findMinDistToBoundary(index2d p, float& dist, int& direction, int& boundPointIndex);
	void mirrorField(vectorField2d& v);
	void zeroFieldOnBound(vectorField2d& v);
	void zeroFieldInsideBound(vectorField2d& v);
	void smoothVelInsideBound(fluid2d& f);
	void solveVelInBoundary(fluid2d& f, unsigned int iterations);
	void writeToFile(string filename);
	void readFromFile(string filename);

	// atributes
	index2d minCorner, maxCorner;
	vector<index2d> points, pointsOnBound, pointsInsideBound;
	vector<boundInnerPoint> pointsNearBound;
	float deltaX, deltaY;
};

class simParam
{
public:
	// constructors
	simParam();
	simParam(int xPoints, int yPoints, float deltaX, float deltaY, float density, float viscosity, float deltaT, float maxAvError, int sudoPressureIt, int realPressureIt, int smoothIt, int maxIt, float initialP, vector2d initialV, string boundaryFilename, string fluidReadFilename, string fluidWriteFilename);

	// operators
	bool const operator== (const simParam& s);
	friend ostream& operator<< (ostream& out, const simParam& s);

	// functions
	void writeToFile(string filename);
	void readFromFile(string filename);
	void initFluid2d(fluid2d& f);

	// atributes
	int xPoints, yPoints, pseudoPressureIt, realPressureIt, smoothIt, maxIt;
	float deltaX, deltaY, density, viscosity, deltaT, maxAvError, initialP;
	vector2d initialV;
	string boundaryFilename, fluidReadFilename, fluidWriteFilename;
};

// functions
void updateVelocity(fluid2d& f, boundary2d& b, float deltaT);
void solvePoisson(scalarField2d& p, scalarField2d& phi, int iterations, bool setFreeStream, bool freeStreamP);
void solvePseudoPressure(fluid2d& f, boundary2d& b, float deltaT, int iterations, float initialP);
void solveRealPressure(fluid2d& f, unsigned int iterations, float initialP);
void simulate(fluid2d& f, boundary2d& b, float deltaT, float maxAvError, int pseudoPressureIt, int realPressureIt, int smoothIt, int maxIt, float initialP, vector2d initialV);