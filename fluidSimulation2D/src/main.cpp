#pragma once

#include "fluidSimulation2d.h"

int main()
{
	cout << "Start simulation" << endl;

	// read simulation parameters
	simParam sp;
	sp.readFromFile("simulation_parameters.txt");
	cout << "Read file" << endl;
	// print simulation parameters to console
	cout << sp << endl;

	// create fluid field f
	fluid2d f(sp);

	// read boundary conditions
	boundary2d b;
	b.deltaX = sp.deltaX;
	b.deltaY = sp.deltaY;

	// if filename is given, load from file
	if (sp.fluidReadFilename != "") f.readFromFile(sp.fluidReadFilename);
	// else, zero the vel inside the bound
	else b.zeroFieldOnBound(f.v);

	b.readFromFile(sp.boundaryFilename);

	// run simulation
	simulate(f, b, sp.deltaT, sp.maxAvError, sp.pseudoPressureIt, sp.realPressureIt, sp.smoothIt, sp.maxIt, sp.initialP, sp.initialV);

	// write f to file
	f.writeToFile(sp.fluidWriteFilename);

	// write b to file as b gets changed by rounding it to nearest grid point
	b.writeToFile(sp.boundaryFilename);

	cout << "End simulation" << endl;
}
