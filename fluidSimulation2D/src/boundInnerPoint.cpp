#include "fluidSimulation2d.h"

boundInnerPoint::boundInnerPoint()
{
	p = index2d(0, 0);
	mp1 = index2d(0, 0);
	mp2 = index2d(0, 0);
	w = 0;
	mw = 0;
}

boundInnerPoint::boundInnerPoint(index2d p, index2d mp1, index2d mp2, float w, float mw)
{
	boundInnerPoint::p = p;
	boundInnerPoint::mp1 = mp1;
	boundInnerPoint::mp2 = mp2;
	boundInnerPoint::w = w;
	boundInnerPoint::mw = mw;
}

void boundInnerPoint::mirrorBoundPoint(vectorField2d& field)
{
	field.points[p.x][p.y] = (field.points[mp1.x][mp1.y] * (1 - mw) + field.points[mp2.x][mp2.y] * mw) * w;
}