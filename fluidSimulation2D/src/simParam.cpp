#include "fluidSimulation2d.h"

// constructors
simParam::simParam(void)
{
	xPoints = 10; yPoints = 10;
	deltaX = 1.0f; deltaY = 1.0f;
	density = 1000.0f; viscosity = 0.0f;
	deltaT = 0.1f; maxAvError = 10.0f;
	pseudoPressureIt = 1000; realPressureIt = 1000;
	smoothIt = 0;
	maxIt = 1000;
	initialP = 0.0f;
	initialV = vector2d(0.0f, 0.0f);
	boundaryFilename = "b.txt";
	fluidReadFilename = "";
	fluidWriteFilename = "f.txt";
}

simParam::simParam(int xPoints, int yPoints, float deltaX, float deltaY, float density, float viscosity, float deltaT, float maxAvError, int pseudoPressureIt, int realPressureIt, int smoothIt, int maxIt, float initialP, vector2d initialV, string boundaryFilename, string fluidReadFilename, string fluidWriteFilename)
{
	simParam::xPoints = xPoints;
	simParam::yPoints = yPoints;
	simParam::deltaX = deltaX;
	simParam::deltaY = deltaY;
	simParam::density = density;
	simParam::viscosity = viscosity;
	simParam::deltaT = deltaT;
	simParam::maxAvError = maxAvError;
	simParam::pseudoPressureIt = pseudoPressureIt;
	simParam::realPressureIt = realPressureIt;
	simParam::smoothIt = smoothIt;
	simParam::maxIt = maxIt;
	simParam::initialP = initialP;
	simParam::initialV = initialV;
	simParam::boundaryFilename = boundaryFilename;
	simParam::fluidReadFilename = fluidReadFilename;
	simParam::fluidWriteFilename = fluidWriteFilename;
}

// operators
bool const simParam::operator== (const simParam& s)
{
	bool out = 1;

	out = out && (xPoints == s.xPoints);
	out = out && (yPoints == s.yPoints);
	out = out && (deltaX == s.deltaX);
	out = out && (deltaY == s.deltaY);
	out = out && (density == s.density);
	out = out && (viscosity == s.viscosity);
	out = out && (deltaT == s.deltaT);
	out = out && (maxAvError == s.maxAvError);
	out = out && (pseudoPressureIt == s.pseudoPressureIt);
	out = out && (realPressureIt == s.realPressureIt);
	out = out && (smoothIt == s.smoothIt);
	out = out && (maxIt == s.maxIt);
	out = out && (initialP == s.initialP);
	out = out && (initialV == s.initialV);
	out = out && (boundaryFilename == s.boundaryFilename);
	out = out && (fluidReadFilename == s.fluidReadFilename);
	out = out && (fluidWriteFilename == s.fluidWriteFilename);

	return out;
}

ostream& operator<< (ostream& out, const simParam& s)
{
	out << "====Simulation parameters====" << endl;
	out << "xPoints         : " << s.xPoints << endl;
	out << "yPoints         : " << s.yPoints << endl;
	out << "deltaX          : " << s.deltaX << endl;
	out << "deltaY          : " << s.deltaY << endl;
	out << "density         : " << s.density << endl;
	out << "viscosity       : " << s.viscosity << endl;
	out << "deltaT          : " << s.deltaT << endl;
	out << "maxAvError      : " << s.maxAvError << endl;
	out << "pseudoPressureIt: " << s.pseudoPressureIt << endl;
	out << "realPressureIt  : " << s.realPressureIt << endl;
	out << "smoothIt        : " << s.smoothIt << endl;
	out << "maxIt           : " << s.maxIt << endl;
	out << "initialP        : " << s.initialP << endl;
	out << "initialV        : " << s.initialV << endl;
	out << "boundaryFilename: " << s.boundaryFilename << endl;
	out << "fluidReadFilename   : " << s.fluidReadFilename << endl;
	out << "fluidWriteFilename   : " << s.fluidWriteFilename << endl;
	return out;
}

// functions
void simParam::writeToFile(string filename) 
{
	ofstream file;
	file.open(filename);

	file << xPoints << endl;
	file << yPoints << endl;
	file << deltaX << endl;
	file << deltaY << endl;
	file << density << endl;
	file << viscosity << endl;
	file << deltaT << endl;
	file << maxAvError << endl;
	file << pseudoPressureIt << endl;
	file << realPressureIt << endl;
	file << smoothIt << endl;
	file << maxIt << endl;
	file << initialP << endl;
	file << initialV.x << " " << initialV.y << endl;
	file << boundaryFilename << endl;
	file << fluidReadFilename << endl;
	file << fluidWriteFilename << endl;

	file.close();
}

void simParam::readFromFile(string filename)
{
	ifstream file;
	file.open(filename);

	string line;
	getline(file, line, '\n');
	xPoints = stoi(line);
	getline(file, line, '\n');
	yPoints = stoi(line);

	getline(file, line, '\n');
	deltaX = stof(line);
	getline(file, line, '\n');
	deltaY = stof(line);

	getline(file, line, '\n');
	density = stof(line);
	getline(file, line, '\n');
	viscosity = stof(line);

	getline(file, line, '\n');
	deltaT = stof(line);
	getline(file, line, '\n');
	maxAvError = stof(line);

	getline(file, line, '\n');
	pseudoPressureIt = stoi(line);
	getline(file, line, '\n');
	realPressureIt = stoi(line);
	getline(file, line, '\n');
	smoothIt = stoi(line);
	getline(file, line, '\n');
	maxIt = stoi(line);

	getline(file, line, '\n');
	initialP = stof(line);
	getline(file, line, ' ');
	initialV.x = stof(line);
	getline(file, line, '\n');
	initialV.y = stof(line);

	getline(file, line, '\n');
	boundaryFilename = line;
	getline(file, line, '\n');
	fluidReadFilename = line;
	getline(file, line, '\n');
	fluidWriteFilename = line;

	file.close();
}